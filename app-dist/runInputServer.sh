#!/bin/bash

# Warning: lib dir determination fails if there is a soft link to this script
#SCRIPT_PATH=my_path=$(dirname "$(cd ${0%/*} && echo $PWD/${0##*/})")
SCRIPT_PATH="."
NATIVE_LIB_DIR="${SCRIPT_PATH}/lib"

java -Dbluecove.jsr82.psm_minimum_off=1 -Djava.library.path="${NATIVE_LIB_DIR}" -jar lib/WiiFlashServerJ.jar
