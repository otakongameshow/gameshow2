//
//  MainViewController.m
//  cuecard2
//
//  Created by Jerry Hsu on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"
#import "ConfigViewController.h"
#import <AudioToolbox/AudioServices.h>

@implementation MainViewController
@synthesize cardLabel = _cardLabel, notificationLabel = _notificationLabel;
@synthesize cardConnection = _cardConnection, notConnection = _notConnection;
@synthesize cardData = _cardData, notData = _notData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    CGRect mainFrame = self.view.bounds;
    mainFrame.size.height -= 80.0;
    self.cardLabel = [[UILabel alloc] initWithFrame: CGRectInset(mainFrame, 10.0, 0.0)];
    self.cardLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.cardLabel.numberOfLines = 0;
    self.cardLabel.lineBreakMode = UILineBreakModeWordWrap;
    self.cardLabel.font = [UIFont boldSystemFontOfSize: 30.0];
    CGRect notFrame = CGRectMake(0.0, mainFrame.size.height, mainFrame.size.width, 80.0);
    self.notificationLabel = [[UILabel alloc] initWithFrame: CGRectInset(notFrame, 10.0, 0.0)];
    self.notificationLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    self.notificationLabel.numberOfLines = 0;
    self.notificationLabel.lineBreakMode = UILineBreakModeWordWrap;
    self.notificationLabel.font = [UIFont boldSystemFontOfSize: 16.0];
    [self.view addSubview: self.cardLabel];
    [self.view addSubview: self.notificationLabel];
    
    UIButton* configButton = [UIButton buttonWithType: UIButtonTypeInfoDark];
    [configButton addTarget: self action: @selector(pushConfig:) forControlEvents: UIControlEventTouchUpInside];
    configButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    CGRect configFrame = configButton.frame;
    configFrame.origin.x = self.view.bounds.size.width - configFrame.size.width;
    configFrame.origin.y = mainFrame.size.height - configFrame.size.height;
    configButton.frame = configFrame;
    [self.view addSubview: configButton];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.cardLabel = nil;
    self.notificationLabel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)pushConfig: (id) control {
    ConfigViewController* con = [[ConfigViewController alloc] init];
    [self presentViewController: con animated: YES completion: nil];
}

- (void) startCardRequest {
    NSString* urlBase = [[NSUserDefaults standardUserDefaults] stringForKey: kPollURL];
    NSURLRequest* request = [NSURLRequest requestWithURL: [NSURL URLWithString: [NSString stringWithFormat: @"%@/q.txt", urlBase]]
                                             cachePolicy: NSURLRequestReloadIgnoringCacheData
                                         timeoutInterval: 1.0];
    self.cardData = [NSMutableData data];
    self.cardConnection = [NSURLConnection connectionWithRequest: request delegate: self];
}

- (void) startNotRequest {
    NSString* urlBase = [[NSUserDefaults standardUserDefaults] stringForKey: kPollURL];
    NSURLRequest* request = [NSURLRequest requestWithURL: [NSURL URLWithString: [NSString stringWithFormat: @"%@/notification.txt", urlBase]]
                                             cachePolicy: NSURLRequestReloadIgnoringCacheData
                                         timeoutInterval: 1.0];
    self.notData = [NSMutableData data];
    self.notConnection = [NSURLConnection connectionWithRequest: request delegate: self];
}

- (void) updateCard: (NSString*) string {
    self.cardLabel.text = string;
}

- (void) updateNot: (NSString*) string {
    if (![self.notificationLabel.text isEqual: string]) {
        self.notificationLabel.text = string;
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}

- (void) setNotColor: (UIColor*) color {
    self.notificationLabel.backgroundColor = color;
}

- (void) startCardRequestOnMain {
    [self performSelectorOnMainThread: @selector(startCardRequest) withObject: nil waitUntilDone: YES];
}

- (void) startNotRequestOnMain {
    [self performSelectorOnMainThread: @selector(startNotRequest) withObject: nil waitUntilDone: YES];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == _cardConnection) {
        [_cardData appendData: data];
    } else if (connection == _notConnection) {
        [_notData appendData: data];
    }
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    [self performSelectorOnMainThread: @selector(setNotColor:) withObject: [UIColor whiteColor] waitUntilDone: NO];
    if (connection == _cardConnection) {
        NSString* string = [[NSString alloc] initWithData: _cardData encoding: NSUTF8StringEncoding];
        [self performSelectorOnMainThread: @selector(updateCard:) withObject: string waitUntilDone: NO];
        [self performSelector: @selector(startCardRequestOnMain) withObject: nil afterDelay: 1.0];
    } else if (connection == _notConnection) {
        NSString* string = [[NSString alloc] initWithData: _notData encoding: NSUTF8StringEncoding];
        [self performSelectorOnMainThread: @selector(updateNot:) withObject: string waitUntilDone: NO];
        [self performSelector: @selector(startNotRequestOnMain) withObject: nil afterDelay: 1.0];
    }
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self performSelectorOnMainThread: @selector(setNotColor:) withObject: [UIColor redColor] waitUntilDone: NO];
    if (connection == _cardConnection) {
        [self performSelector: @selector(startCardRequestOnMain) withObject: nil afterDelay: 1.0];
    } else if (connection == _notConnection) {
        [self performSelector: @selector(startNotRequestOnMain) withObject: nil afterDelay: 1.0];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    [self startCardRequest];
    [self startNotRequest];
}

@end
