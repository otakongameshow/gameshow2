//
//  main.m
//  cuecard2
//
//  Created by Jerry Hsu on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"cuecard2AppDelegate");
        return retVal;
    }
}
