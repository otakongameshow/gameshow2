//
//  ConfigViewController.h
//  cuecard2
//
//  Created by Jerry Hsu on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString* kPollURL;

@interface ConfigViewController : UIViewController <UITextFieldDelegate> {
    UITextField* _urlField;
}

@property (nonatomic, strong) UITextField* urlField;


@end
