//
//  MainViewController.h
//  cuecard2
//
//  Created by Jerry Hsu on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController {
    UILabel* _cardLabel;
    UILabel* _notificationLabel;
    NSURLConnection* _cardConnection;
    NSURLConnection* _notConnection;
    NSMutableData* _cardData;
    NSMutableData* _notData;
}

@property (nonatomic, strong) UILabel* cardLabel;
@property (nonatomic, strong) UILabel* notificationLabel;
@property (nonatomic, strong) NSURLConnection* cardConnection;
@property (nonatomic, strong) NSURLConnection* notConnection;
@property (nonatomic, strong) NSMutableData* cardData;
@property (nonatomic, strong) NSMutableData* notData;


@end
