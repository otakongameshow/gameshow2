//
//  cuecard2AppDelegate.h
//  cuecard2
//
//  Created by Jerry Hsu on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cuecard2AppDelegate : NSObject <UIApplicationDelegate> {
    UIViewController* _mainCon;
}

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) UIViewController* mainCon;

@end
