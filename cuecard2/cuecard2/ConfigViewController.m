//
//  ConfigViewController.m
//  cuecard2
//
//  Created by Jerry Hsu on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ConfigViewController.h"

NSString* kPollURL = @"poll URL";

@implementation ConfigViewController
@synthesize urlField = _urlField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    CGRect labelFrame = CGRectInset(CGRectMake(0.0, 10.0, self.view.bounds.size.width, 44.0), 10.0, 0.0);
    UILabel* urlLabel = [[UILabel alloc] initWithFrame: labelFrame];
    urlLabel.text = @"Poll URL";
    urlLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    CGRect textFrame = labelFrame;
    textFrame.origin.y += 44.0;
    self.urlField = [[UITextField alloc] initWithFrame: textFrame];
    self.urlField.borderStyle = UITextBorderStyleRoundedRect;
    self.urlField.text = [[NSUserDefaults standardUserDefaults] stringForKey: kPollURL];
    self.urlField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    self.urlField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.urlField.autocorrectionType = UITextAutocorrectionTypeNo;
    CGRect doneFrame = textFrame;
    doneFrame.origin.y += 44.0;
    UIButton* doneButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [doneButton addTarget: self action: @selector(doneTapped:) forControlEvents: UIControlEventTouchUpInside];
    [doneButton setTitle: @"Done" forState: UIControlStateNormal];
    doneButton.frame = doneFrame;
    
    [self.view addSubview: urlLabel];
    [self.view addSubview: self.urlField];
    [self.view addSubview: doneButton];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.urlField = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)doneTapped: (id) con {
    [[NSUserDefaults standardUserDefaults] setObject: self.urlField.text forKey: kPollURL];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.presentingViewController dismissViewControllerAnimated: YES completion: nil];
}


@end
