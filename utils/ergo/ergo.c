/*
    Linux Ergodex DX1 "Driver"
    Copyright (C) 2007 Richard A Burton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Credits for protocol information to Hydra
// http://ergodex.hydraproductions.com/wiki

#include <usb.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>

#include "ergo.h"

#define MAJOR 0
#define MINOR 1
#define BUILD 2
#define YEARS "2007"

#define TIMEOUT 1000
#define RC_NOMATCH 1000

// function prototypes
void dump_block(unsigned char*, int);
int read_keys(char*, unsigned char[][2]);
int write_req(usb_dev_handle *, void *, int);
int read_resp(usb_dev_handle*, void*, int, int);
int pad_enable(usb_dev_handle*);
int pad_disable(usb_dev_handle*);
int pad_query_device(usb_dev_handle*);
int pad_program_keys(usb_dev_handle*, unsigned char[][2]);
struct usb_dev_handle *pad_open(struct usb_device *);
void pad_close(struct usb_dev_handle*);
int cmd_list(struct usb_device*);
int cmd_reset(struct usb_device*);
int cmd_monitor(struct usb_device*);
int cmd_program(struct usb_device*, unsigned char[][2]);
void usage(char*);
int process_cmdline(int, char*[]);
void msg(int, char *, ...);
void handle_macro(ergo_resp_macro* macro);
void handle_status(ergo_resp_status* status);
void handle_signal(int);

// command line args after processing
struct _args {
   int show;
   int reset;
   int monitor;
   int program;
   int verbosity;
   char *file;
   unsigned long long device;
   int devfound;
   int flash;
} args;

int signalled = 0;

// write out a message
// levels below 1 are always displayed (0 stdout, <0 stderr)
// levels above 0 displayed according to verbosity level (to stderr)
void msg(int level, char *fmt, ...) {
   va_list argp;
   if (args.verbosity >= level || level < 0) {
      va_start(argp, fmt);
      vfprintf((level == 0 ? stdout : stderr), fmt, argp);
      va_end(argp);
   }
}

void dump_block(unsigned char *data, int sent) {
   msg(-1, (sent ? "Data out: " : "Data in:  "));
   msg(-1, "%02hhx%02hhx %02hhx%02hhx  ", data[0],data[1],data[2],data[3]);
   msg(-1, "%02hhx%02hhx %02hhx%02hhx  ", data[4],data[5],data[6],data[7]);
   msg(-1, "%02hhx%02hhx %02hhx%02hhx  ", data[8],data[9],data[10],data[11]);
   msg(-1, "%02hhx%02hhx %02hhx%02hhx\n", data[12],data[13],data[14],data[15]);
}

// read keys from file
int read_keys(char *filename, unsigned char keys[][2]) {

   char line[20];
   int key, type, value;
   FILE *file;

   file = fopen(filename, "r");
   if (!file) return -1;

   while (fgets(line, 20, file)) {
      if (line[0] == '\n' || line[0] == '#') continue;
      if (sscanf(line, "%d,%d,%d", &key, &type, &value) == 3) {
         keys[key-1][0] = (unsigned char)type;
         if (type != 3) keys[key-1][1] = (unsigned char)value;
         msg(2, "key: %d, type: %d, value: %d\n", key, type, value);
      }
   }

   return 0;
}

int write_req(usb_dev_handle *pad, void *data, int len) {
   int rc = usb_interrupt_write(pad, ERGO_ENDPOINT_OUT, data, len, TIMEOUT);
   if (rc != len) msg(-1, "error writing: %d\n", rc);
   if (args.verbosity > 2 || rc != len) {
      dump_block(data, 1);
   }
   usleep(10*1000);
   return rc;
}

int read_resp(usb_dev_handle *pad, void *data, int len, int quiet) {
   int rc = usb_interrupt_read(pad, ERGO_ENDPOINT_IN, data, len, TIMEOUT);
   if (rc != len && (!quiet || rc != -110)) msg(-1, "error reading: %d\n", rc);
   if (args.verbosity > 2 || (rc != len && rc > 0)) {
      dump_block(data, 0);
   }
   return rc;
}

// gets a handle to the pad, NULL in case of error
struct usb_dev_handle *pad_open(struct usb_device *dev) {

   int rc;
   struct usb_dev_handle *pad;

   // open it
   pad = usb_open(dev);
   if (!pad) {
      msg(-1, "failed to open\n");
      return NULL;
   }

   // claim the interface
   rc = usb_claim_interface(pad, ERGO_INTERFACE);
   if (rc) {
      msg(-1, "error claiming: %d\n", rc);
      usb_close(pad);
      return NULL;
   }

   return pad;
}

void pad_close(struct usb_dev_handle *pad) {

   int rc;

   // release the interface
   rc = usb_release_interface(pad, ERGO_INTERFACE);
   if (rc) msg(-1, "error releasing: %d\n", rc);

   // close the device
   usb_close(pad);
}

// enable the pad (after programming)
int pad_enable(usb_dev_handle *pad) {

   int rc;
   ergo_req_set req = {0};

   req.type     = ERGO_TYPE_SET;
   req.leds     = 0x01;
   req.unknown3 = 0x01;
   req.unknown4 = 0x01;

   rc = write_req(pad, &req, 16);
   if (rc != 16) return -1;

   return 0;
}

// disable the pad (to allow for programming)
int pad_disable(usb_dev_handle *pad) {

   int rc;
   ergo_req_set req = {0};

   req.type     = ERGO_TYPE_SET;
   req.leds     = 0x01;
   req.disabled = 0x01;
   req.unknown3 = 0x01;
   req.unknown4 = 0x01;

   rc = write_req(pad, &req, 16);
   if (rc != 16) return -1;

   return 0;
}

// get device information
// returns 0 on sucess
// returns <0 on error
// returns RC_NOMATCH if args requires specific device and this isn't it
int pad_query_device(usb_dev_handle *pad) {

   int rc;
   ergo_req_device req = {0};
   ergo_resp_device resp;
   struct usb_device *dev;

   req.type = ERGO_TYPE_DEVICE;

   rc = write_req(pad, &req, 16);
   if (rc != 16) return -1;

   // get the response
   rc = read_resp(pad, &resp, 16, 0);
   if (rc != 16) return -2;
   if (resp.type != ERGO_TYPE_DEVICE) return -3;

   // display the results
   if (args.verbosity || args.show) {
      dev = usb_device(pad);
      msg((args.show ? 0 : 1),
          "Bus: %s, Device: %s, ID: 0x%llx, Version: %hhd.%hhd\n",
          dev->bus->dirname, dev->filename,
          resp.serial, resp.version[0], resp.version[1]);
   }

   // if looking for specific device check for a match
   if (args.device) {
      if (resp.serial != args.device) return RC_NOMATCH;
      args.devfound = 1;
   }

   return 0;
}

// program the keys
int pad_program_keys(usb_dev_handle *pad, unsigned char keys[][2]) {

   int rc, i, next = 0;
   ergo_req_program req = {0};

   req.type = ERGO_TYPE_PROGRAM;

   // disable it
   if (pad_disable(pad)) return -1;

   // loop through key list
   for (i = 0; i < ERGO_KEYS; i++) {
      // is key set?
      if (keys[i][0] != 0) {
         req.keys[next][0] = i+1;
         req.keys[next][1] = keys[i][0];
         req.keys[next][2] = keys[i][1];
         next++;
         msg(1, "Setting key %02d, type %d, value %02d\n",
             i+1, keys[i][0], keys[i][1]);
         // when we have a full set of 5 write them
         if (next >= 5) {
            rc = write_req(pad, &req, 16);
            if (rc != 16) return -2;
            // clear request
            memset(req.keys, 0, 5*3);
            next = 0;
         }
      }
   }

   // write any last ones
   if (next > 0) {
      rc = write_req(pad, &req, 16);
      if (rc != 16) return -3;
   }

   // re-enable it
   if (pad_enable(pad)) return -4;

   return 0;
}

// flash the leds
int pad_flash(usb_dev_handle *pad, unsigned char leds) {

   int rc;
   int i;
   ergo_req_set req = {0};

   req.type     = ERGO_TYPE_SET;
   req.disabled = 0x00;
   req.unknown3 = 0x01;
   req.unknown4 = 0x01;

   for (i=0; i < 10; i++) {
      req.leds     = i % 2 ?leds:0;
      rc = write_req(pad, &req, 16);
      if (rc != 16) return -1;
      usleep(250000);
   }

   return 0;
}

// reset usb device, since adding the sleep after sending
// it hasn't broken again, but useful if it does
int cmd_reset(struct usb_device *dev) {
   struct usb_dev_handle *pad = usb_open(dev);
   if (pad) {
      usb_reset(pad);
      msg(1, "device reset\n");
   } else {
      msg(-1, "failed to reset\n");
   }
}

int cmd_program(struct usb_device *dev, unsigned char keys[][2]) {

   int rc = 0;
   struct usb_dev_handle *pad;

   pad = pad_open(dev);
   if (!pad) return -1;

   // query the pad (to match device id)
   if (args.device) {
      rc = pad_query_device(pad);
      if (rc < 0) msg(-1, "error querying device: %d\n", rc);
   }

   // rc will be >0 if match failed, <0 if error checking
   if (!rc) {
      // program keys
      rc = pad_program_keys(pad, keys);
      if (rc) msg(-1, "failed to program keys: %d\n", rc);
   }

   pad_close(pad);

   return rc;
}

int cmd_flash(struct usb_device *dev, unsigned char leds ) {

   int rc = 0;
   struct usb_dev_handle *pad;

   pad = pad_open(dev);
   if (!pad) return -1;

   // query the pad (to match device id)
   if (args.device) {
      rc = pad_query_device(pad);
      if (rc < 0) msg(-1, "error querying device: %d\n", rc);
   }

   // rc will be >0 if match failed, <0 if error checking
   if (!rc) {
      // program keys
      rc = pad_flash(pad, leds);
      if (rc) msg(-1, "failed to flash leds: %d\n", rc);
   }

   pad_close(pad);

   return rc;
}

int cmd_list(struct usb_device *dev) {

   int rc;
   struct usb_dev_handle *pad;

   pad = pad_open(dev);
   if (!pad) return -1;

   // query the pad
   rc = pad_query_device(pad);
   if (rc) msg(-1, "error querying device: %d\n", rc);

   pad_close(pad);
}

void handle_signal(int sig) {
   msg(-1, "Interrupting...\n");
   signalled = 1;
}

// returns 0 if sucessfully monitored this device
// returns <0 on error
// returns RC_NOMATCH if no match to specific device
int cmd_monitor(struct usb_device *dev) {
   int rc, go = 1;
   ergo_resp resp;
   struct usb_dev_handle *pad;

   void (*old_int)(int);
   void (*old_term)(int);

   pad = pad_open(dev);

   // query the pad (to match device id)
   if (args.device) {
      rc = pad_query_device(pad);
      if (rc < 0) msg(-1, "error querying device: %d\n", rc);
      if (rc != 0) return rc;
   }

   msg(0, "Monitoring...\n");

   // setup signal handlers
   old_int = signal(SIGINT, handle_signal);
   old_term = signal(SIGTERM, handle_signal);

   while (go & !signalled) {
      rc = read_resp(pad, &resp, 16, 1);
      if (rc != 16) {
         if (rc != -110) go = 0;
      } else {
         switch (resp.type) {
         case ERGO_TYPE_MACRO:
            handle_macro((ergo_resp_macro*)&resp);
            break;
         case ERGO_TYPE_STATUS:
            handle_status((ergo_resp_status*)&resp);
            break;
         default:
            msg(-1, "unexpected response, type %d\n", resp.type);
         }
      }
   }

   // reset signal stuff
   signalled = 0;
   signal(SIGINT, old_int);
   signal(SIGTERM, old_term);

   return 0;
}

void handle_macro(ergo_resp_macro* macro) {
   msg(0, "macro for keys %d %d %d %d %d %d\n", macro->keys[0], macro->keys[1],
       macro->keys[2], macro->keys[3], macro->keys[4], macro->keys[5]);
}

void handle_status(ergo_resp_status* status) {
   msg(0, "status update keys: 0x%02hhx, leds: 0x%02hhx, buttons: 0x%02hhx\n",
       status->keys, status->leds, status->buttons);
}

void usage(char* prog) {
   msg(-1, "Usage:\n");
   msg(-1, " %s <options> <command>\n", prog);
   msg(-1, "Options:\n");
   msg(-1, " -v verbose (can be specified more than once)\n");
   msg(-1, " -d <deviceid> operate on specified device only, valid with -p/-m\n");
   msg(-1, "Commands:\n");
   msg(-1, " -h help\n");
   msg(-1, " -l list devices\n");
   msg(-1, " -m monitor first or specified device\n");
   msg(-1, " -r reset usb interface\n");
   msg(-1, " -p <filename> program with keys from file\n");
   msg(-1, " -f <1-3> flash LEDs on device\n");
   msg(-1, "Notes:\n");
   msg(-1, " -d can only be used with -p\n");
}

int process_cmdline(int argc, char *argv[]) {

   int opt, commands = 0;
   const char* optstr = "f:p:d:lmrvh?";

   // blank the args
   memset(&args, 0, sizeof(args));

   opt = getopt(argc, argv, optstr);
   while (opt != -1) {
      switch (opt) {
      case 'd':
         args.device = strtoull(optarg, (char **)NULL, 16);
         break;
      case 'f':
         commands++;
         args.flash = strtoull(optarg, (char **)NULL, 10);
         break;
      case 'p':
         commands++;
         args.program = 1;
         args.file = optarg;
         break;
      case 'm':
         commands++;
         args.monitor = 1;
         break;
      case 'l':
         commands++;
         args.show = 1;
         break;
      case 'r':
         commands++;
         args.reset = 1;
         break;
      case 'v':
         args.verbosity++;
         break;
      case 'h':
      case '?':
         usage(argv[0]);
         return -1;
      }
      opt = getopt(argc, argv, optstr);
   }

   // check only one command was specified
   if (commands != 1) {
      usage(argv[0]);
      return -1;
   }

   // -d only useful with -p & -m
   if (args.device && (!args.program && !args.monitor)) {
      usage(argv[0]);
      return -1;
   }

   return 0;
}

int main(int argc, char *argv[]) {

   int found = 0;

   struct usb_bus *busses;
   struct usb_bus *bus;
   struct usb_device *dev;

   unsigned char keys[ERGO_KEYS][2];

   // display banner
   msg(0, "(Unofficial) Ergodex DX1 Config Tool v%0d.%0d.%0d\n",
       MAJOR, MINOR, BUILD);
   msg(0, "Copyright %s Richard A Burton, released under GPL v2.\n", YEARS);
   msg(0, "\n");

   // setup libusb stuff
   usb_init();
   usb_find_busses();
   usb_find_devices();
   busses = usb_get_busses();

   // process command line
   if (process_cmdline(argc, argv)) return -1;

   // load keys from file, if in programming mode
   if (args.program) {
      memset(keys, 0, ERGO_KEYS*2);
      if (read_keys(args.file, keys)) {
         msg(-1, "failed to load keys\n");
         return -1;
      }
   }

   // find device
   for (bus = busses; bus; bus = bus->next) {
      for (dev = bus->devices; dev; dev = dev->next) {
         if (dev->descriptor.idVendor == ERGO_VID &&
             dev->descriptor.idProduct == ERGO_PID) {

            // got one
            msg(1, "found a DX1 at %s:%s\n", bus->dirname, dev->filename);
            found++;

            // what should we do with it?
            if (args.reset) {
               cmd_reset(dev);
            } else if (args.program) {
               cmd_program(dev, keys);
            } else if (args.show) {
               cmd_list(dev);
            } else if (args.monitor) {
               cmd_monitor(dev);
            } else if (args.flash) {
               cmd_flash(dev, args.flash);
            }

         }
      }
   }

   if (found < 1) {
      msg(-1, "no devices found!\n");
      return -1;
   } else if (args.device && !args.devfound) {
      msg(-1, "could not find device with id %llx\n", args.device);
      return -1;
   }

   return 0;
}

