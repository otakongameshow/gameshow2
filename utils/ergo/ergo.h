/*
    Linux Ergodex DX1 "Driver"
    Copyright (C) 2007 Richard A Burton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Credits for protocol information to Hydra
// http://ergodex.hydraproductions.com/wiki

#ifndef __ERGO_H__
#define __ERGO_H__

#define ERGO_VID 0x1603
#define ERGO_PID 0x0002

#define ERGO_KEYS 50

#define ERGO_INTERFACE 1

#define ERGO_ENDPOINT_OUT 0x02
#define ERGO_ENDPOINT_IN  0x82

#define ERGO_TYPE_STATUS  0x01
#define ERGO_TYPE_SET     0x02
#define ERGO_TYPE_PROGRAM 0x03
#define ERGO_TYPE_DEVICE  0x0a
#define ERGO_TYPE_MACRO   0x02
#define ERGO_TYPE_TEST    0x03

#pragma pack(1)

// request structures
struct _ergo_req {
    unsigned char type;
    unsigned char rest[15];
} typedef ergo_req;

typedef ergo_req ergo_req_status; // 0x01 (no data)
typedef ergo_req ergo_req_device; // 0x0a (no data)

struct _ergo_req_set {
    unsigned char type; //0x02
    unsigned char unknown1;
    unsigned char test;
    unsigned char leds;
    unsigned char unknown2;
    unsigned char disabled;
    unsigned char unknown3;
    unsigned char unknown4;
    unsigned char rest[8];
} typedef ergo_req_set;

struct _ergo_req_program {
    unsigned char type; //0x03
    unsigned char keys[5][3];
} typedef ergo_req_program;


// response structures
struct _ergo_resp {
    unsigned char type;
    unsigned char rest[15];
} typedef ergo_resp;

struct _ergo_resp_status {
    unsigned char type; // 0x01
    unsigned char unknown1;
    unsigned char unknown2;
    unsigned char leds;
    unsigned char buttons;
    unsigned char keys;
    unsigned char rest[10];
} typedef ergo_resp_status;

struct _ergo_resp_macro {
    unsigned char type; // 0x02
    unsigned char unknown1;
    unsigned char unknown2;
    unsigned char keys[6];
    unsigned char rest[7];
} typedef ergo_resp_macro;

typedef ergo_resp_macro ergo_resp_test; // 0x03 (same as 0x02)

struct _ergo_resp_device {
    unsigned char type; // 0x0a
    unsigned long long serial;
    unsigned char version[2];
    unsigned char rest[5];
} typedef ergo_resp_device;

#pragma pack()

#endif

