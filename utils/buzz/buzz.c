
#include <usb.h>
#include <stdio.h>
#include <string.h>

#define VENDOR_ID 0x054c
#define PRODUCT_ID 0x1000

int main(int argc, char *argv[]) {
    struct usb_bus *busses;
    struct usb_bus *bus;
    struct usb_device *dev;
    usb_dev_handle *buzz;
    int lamps = 0xf;
    int done = 0;

    usb_init();
    usb_find_busses();
    usb_find_devices();

    busses = usb_get_busses();

    for (bus = busses; bus; bus = bus->next) {
        for (dev = bus->devices; dev; dev = dev->next) {
            /* Look for buzz device */
            if (dev->descriptor.idVendor == VENDOR_ID &&
            dev->descriptor.idProduct == PRODUCT_ID) {
                printf("Found buzz at %s:%s\n", bus->dirname, dev->filename);
                buzz = usb_open(dev);

                while (!done) {
                    char report[8];
                    report[0] = 0;
                    report[1] = lamps & 0x1 ? 0xff : 0x00;
                    report[2] = lamps & 0x2 ? 0xff : 0x00;
                    report[3] = lamps & 0x4 ? 0xff : 0x00;
                    report[4] = lamps & 0x8 ? 0xff : 0x00;
                    report[5] = 0;
                    report[6] = 0;
                    report[7] = 0;

int i = 0;

                    int ret = usb_control_msg(buzz,
                        USB_ENDPOINT_OUT + USB_TYPE_CLASS + USB_RECIP_INTERFACE,
                        0x9, 0x2 << 8, i, report, 8, 500);
/*
                        0x9, 0x2 << 8, i, report, 8, 100);
*/
		printf("0x%x = 0x%x\n", i, ret);

                    char inbuffer[256];
                    if (fgets(inbuffer,256,stdin) == NULL) {
                        done = 1;
                    } else {
                        if (inbuffer[0] == 'q') {
                            done = 1;
                        } else {
                            lamps = strtol(inbuffer, (char **)NULL, 0);
                        }
                    }
                }
                usb_close(buzz);
		done = 0;
/*                return 0; */
            }
        }
    }
    printf("No buzz devices found.\n");
    return 0;
}
