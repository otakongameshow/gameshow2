package com.otakon.gameshow.wiiflashserverj;

import java.util.*;

import wiiremotej.event.WRButtonEvent;
import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.EventQueue;
import net.java.games.input.Event;

import com.tojio.wiiflashserverj.WiiFlashServerJ;

/**
  * JInput Poll Thread
  * 
  * @author jhsu
  * 
  */
public class JInputKBPollThread {
  private final String CLASS_NAME="JInputKBPollThread";
  private WiiFlashServerJ wfs;

  static final long HEARTBEATMS =100; // 100ms
  ArrayList<Controller> controllers;
  BuzzMoteAdapter kbHandles[];
  int currentButtonState[] = new int[4];
  Map<String, Integer> keyMap;
    int buttonToEventMap[] = {
      WRButtonEvent.A, 
        WRButtonEvent.UP,
        WRButtonEvent.RIGHT,
        WRButtonEvent.DOWN,
        WRButtonEvent.LEFT
      };

      /**
        * Standard constructor
        * 
        */
      public JInputKBPollThread( WiiFlashServerJ myWfs, ArrayList<Controller> myControllers, BuzzMoteAdapter[] myKBHandles ) {
        wfs = myWfs;
        keyMap = new HashMap<String, Integer>();
        keyMap.put("1", new Integer(0));
        keyMap.put("2", new Integer(1));
        keyMap.put("3", new Integer(2));
        keyMap.put("4", new Integer(3));
        keyMap.put("5", new Integer(4));
        keyMap.put("Q", new Integer(5));
        keyMap.put("W", new Integer(6));
        keyMap.put("E", new Integer(7));
        keyMap.put("R", new Integer(8));
        keyMap.put("T", new Integer(9));
        keyMap.put("A", new Integer(10));
        keyMap.put("S", new Integer(11));
        keyMap.put("D", new Integer(12));
        keyMap.put("F", new Integer(13));
        keyMap.put("G", new Integer(14));
        keyMap.put("Z", new Integer(15));
        keyMap.put("X", new Integer(16));
        keyMap.put("C", new Integer(17));
        keyMap.put("V", new Integer(18));
        keyMap.put("B", new Integer(19));
        controllers = myControllers;
        kbHandles = myKBHandles;
        currentButtonState[0] = 0;
        currentButtonState[1] = 0;
        currentButtonState[2] = 0;
        currentButtonState[3] = 0;

        new Thread(new Runnable() {
          public void run () {
            try {
              while (true) {
                for (int i = 0; i < controllers.size(); i++) {
                  Controller controller = (Controller)controllers.get(i);
                  if (!controller.poll()) {
                    wfs.log (CLASS_NAME + " " + i + " poll failed");
                    return;
                  }
                  EventQueue event_queue = controller.getEventQueue();
                  Event event = new Event();
                  while (event_queue.getNextEvent(event)) {
                    Component component = event.getComponent();
                    String id = component.getIdentifier().toString();
                    float data = event.getValue();
                    wfs.log (CLASS_NAME + " Event: " 
                      + component.getName() + "("+ id +") = "
                      + data);
                    Integer key = (Integer)keyMap.get(id);
                    if (key == null) {
                      // No mapping, get next event
                      continue;
                    }
                    int buzzId = key.intValue()/5;
                    int button = key.intValue()%5;
                    if (data == 0.0f) {
                      // Clear bit
                      currentButtonState[buzzId] &= ~buttonToEventMap[button];
                      wfs.log (CLASS_NAME + " " + buzzId + " clearing " + button + " state = " + currentButtonState[buzzId]);
                    } else if (data == 1.0f) {
                      // Set bit
                      currentButtonState[buzzId] |= buttonToEventMap[button];
                      wfs.log (CLASS_NAME + " " + buzzId + " setting  " + button + " state = " + currentButtonState[buzzId]);
                    } else {
                      wfs.log (CLASS_NAME + " data was not 0 or 1 = " + data);
                    }
                    // I don't actually want to pass data as a wrbuttonevent.
                    // Instead, pass as bitmask and have buzzmoteadapter parse
                    kbHandles[buzzId].combinedInputReceived(currentButtonState[buzzId]);
                  }

                  Thread.sleep(HEARTBEATMS);
                }
              }
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
          }).start();

        }

      }
