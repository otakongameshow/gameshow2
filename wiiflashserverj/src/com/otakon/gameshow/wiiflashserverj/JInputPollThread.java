package com.otakon.gameshow.wiiflashserverj;

import java.io.*;

import wiiremotej.event.WRButtonEvent;
import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.EventQueue;
import net.java.games.input.Event;
import java.util.HashMap;

import com.tojio.wiiflashserverj.WiiFlashServerJ;

class StreamGobbler extends Thread
{
    InputStream is;
    String type;
    
    StreamGobbler(InputStream is, String type)
    {
        this.is = is;
        this.type = type;
    }
    
    public void run()
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line=null;
            while ( (line = br.readLine()) != null)
                System.out.println(type + ">" + line);    
            } catch (IOException ioe)
              {
                ioe.printStackTrace();  
              }
    }
}

/**
 * JInput Poll Thread
 * 
 * @author jhsu
 * 
 */
public class JInputPollThread {
  private final String CLASS_NAME="JInputPollThread";
  private WiiFlashServerJ wfs;
  private Process lampProc;
  private OutputStreamWriter out;
  // private BufferedInputStream inStream;

  static final long HEARTBEATMS =100; // 100ms
  static final long SLOW_ON=500;
  static final long SLOW_OFF=500;
  static final long FAST_ON=250;
  static final long FAST_OFF=250;
  Controller controller;
  BuzzMoteAdapter buzzHandles[];
  WRButtonEvent lastButtonEvents[] = new WRButtonEvent[4];
  int currentButtonState[] = new int[4];
  int buttonToEventMap[] = {
    WRButtonEvent.A, 
    WRButtonEvent.LEFT,
    WRButtonEvent.DOWN,
//      WRButtonEvent.PLUS,
//      WRButtonEvent.MINUS,
    WRButtonEvent.RIGHT,
    WRButtonEvent.UP
  };
  private HashMap<Component, Integer> buttonNameIdMap;

  /**
   * Standard constructor
   * 
   */
  public JInputPollThread( WiiFlashServerJ myWfs, Controller myController, BuzzMoteAdapter[] myBuzzHandles ) {
    wfs = myWfs;
    controller = myController;
    buzzHandles = myBuzzHandles;
    currentButtonState[0] = 0;
    currentButtonState[1] = 0;
    currentButtonState[2] = 0;
    currentButtonState[3] = 0;

    // Try to find lamp setting binary
    Runtime runtime = Runtime.getRuntime();
    try {
      wfs.log (CLASS_NAME + " Starting buzz lamp control");
      lampProc = runtime.exec("./mac-wbuzz-activator");
    } catch (Exception e) {
      wfs.log (CLASS_NAME + " Buzz lamp control binary not found");
      lampProc = null;
    }
    if (lampProc != null) {
      StreamGobbler errorGobbler = new StreamGobbler(lampProc.getErrorStream(), "ERROR");
      StreamGobbler outputGobbler = new StreamGobbler(lampProc.getInputStream(), "OUTPUT");
    
      errorGobbler.start();
      outputGobbler.start();
    
      wfs.log (CLASS_NAME + " Getting pipe to lamp control");
      out = new OutputStreamWriter(lampProc.getOutputStream());
    }
    this.buttonNameIdMap = createNameIdMap(controller);

    new Thread(new Runnable() {
      public void run () {
        try {
          int count = 0;
          int lampControl = 0;
          int lastLampControl = -1;
	  wfs.log(CLASS_NAME + " Started poll thread");
          while (true) {
            if (!controller.poll()) {
              wfs.log (CLASS_NAME + " poll failed");
              return;
            }
            EventQueue event_queue = controller.getEventQueue();
            Event event = new Event();
            while (event_queue.getNextEvent(event)) {
              Component component = event.getComponent();
              int id=-1;
              try {
                id = Integer.parseInt(component.getIdentifier().toString());
              } catch (NumberFormatException nfe) {
                wfs.log(CLASS_NAME+" translating non-numeric identifier: "+component.getIdentifier());
                id = buttonNameIdMap.get(component).intValue();
              }
              float data = event.getValue();
              wfs.log (CLASS_NAME + " Event: " 
                 + component.getName() + "("+ id +") = "
                 + data);
              // id div 5 should correspond to the buzz controller.
              // id mod 5 = 0:Red button, 4: blue(a), 3: orange(b), 2: green(c), 1: yellow(d)
              int buzzId = id/5;
              int button = id%5;
              
              if (buzzId > 3) {
                wfs.log(CLASS_NAME+" skipping invalid buzzId: "+buzzId);
                continue;
              }
              if (data == 0.0f) {
                // Clear bit
                currentButtonState[buzzId] &= ~buttonToEventMap[button];
                wfs.log (CLASS_NAME + " " + buzzId + " clearing " + button + " state = " + currentButtonState[buzzId]);
              } else if (data == 1.0f) {
                // Set bit
                currentButtonState[buzzId] |= buttonToEventMap[button];
                wfs.log (CLASS_NAME + " " + buzzId + " setting  " + button + " state = " + currentButtonState[buzzId]);
              } else {
                wfs.log (CLASS_NAME + " data was not 0 or 1 = " + data);
              }
              // I don't actually want to pass data as a wrbuttonevent.
              // Instead, pass as bitmask and have buzzmoteadapter parse
              buzzHandles[buzzId].combinedInputReceived(currentButtonState[buzzId]);
            }
            
            //handle all lamps
            lampControl = 0;
            for (int i = 0; i<4; i += 1) {
              int lampState = buzzHandles[i].getLEDLights();
              if (lampState == 1) {
                lampControl += 1 << i;
              } else if (lampState == 2) {
                if (count % ((SLOW_ON+SLOW_OFF)/HEARTBEATMS) < (SLOW_ON/HEARTBEATMS)) {
                  lampControl += 1 << i;
                }
              } else if (lampState == 3) {
                if (count % ((FAST_ON+FAST_OFF)/HEARTBEATMS) < (FAST_ON/HEARTBEATMS)) {
                  lampControl += 1 << i;
                }
              }
            }
            if (lampControl != lastLampControl) {
              if (lampProc != null) {
                out.write(lampControl + "\n");
                out.flush();
              }
            }            
            lastLampControl = lampControl;
            
            Thread.sleep(HEARTBEATMS);
            count += 1;
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }).start();

  }
  
  private HashMap<Component, Integer> createNameIdMap(Controller controller) {
    HashMap<Component,Integer> nameIdMap = new HashMap<Component,Integer>();
    
    Component components[] = controller.getComponents();
    for (int i=0; i<components.length; i++) {
      nameIdMap.put(components[i], Integer.valueOf(i));
    }
    
    return nameIdMap;
  }

}
