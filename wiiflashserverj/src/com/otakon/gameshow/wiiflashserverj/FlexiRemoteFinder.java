package com.otakon.gameshow.wiiflashserverj;

import java.util.*;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;

import wiiremotej.event.WiiRemoteDiscoveredEvent;

import com.tojio.wiiflashserverj.WiiFlashServerJ;
import com.tojio.wiiflashserverj.WiiMoteFinder;

/**
  * Extension of WiiMoteFinder that can handle multiple selectable controller
  * inputs
  * 
  * Javadoc for WiiRemoteJ API at:
* http://web-mime.com/manuals/java/wiiremotej/v-1_2/
* 
  * @author jkao
  * 
  */
public class FlexiRemoteFinder extends WiiMoteFinder {

  // used for log
  private final String CLASS_NAME = "WiiMoteFinder";
  private WiiFlashServerJ wfs;

  /**
    * 
    * @param wfs
    *          , the instance of WiiFlashServerJ to put remotes into
    */
  public FlexiRemoteFinder(WiiFlashServerJ wfs) {
    // Gotta do this to make sure that the superclass is inited
    // but this is useless to us because we can't access the private
    // variable the parent is putting wfs into
    super(wfs);

    // So, we gotta store it ourselves.
    this.wfs = wfs;

    // TODO: Feed wfs.addRemoteAdapter() with BuzzMoteAdapters, KeyboardAdapters here
    // Make sure the moteIDs for the adapter go from 0 to 3.

    // Get all JInput devices
    ControllerEnvironment ce = ControllerEnvironment.getDefaultEnvironment();
    Controller[] ca = ce.getControllers();
    JInputPollThread pollThread = null;
    JInputKBPollThread kbPollThread = null;
    Controller buzzController = null;
    ArrayList<Controller> kbControllers = new ArrayList<Controller>();

    // Search through devices for Wbuzz or Buzz name
    for (int i=0; i< ca.length; i++) {
      if ((ca[i].getName().compareTo("Wbuzz") == 0) ||
      (ca[i].getName().endsWith("Buzz"))) {
        buzzController = ca[i];
        wfs.log(CLASS_NAME + " Found buzz controller named " + buzzController.getName());
        break;
      }
    }
    // If we found one, then create buzzmoteadapter instances for it,
    // create Jinput poll thread and
    // pass bma instances to addRemoteAdapter()
    if (buzzController != null) {
      BuzzMoteAdapter buzzHandles[] = new BuzzMoteAdapter[4];
      buzzHandles[0] = new BuzzMoteAdapter(wfs,0);
      buzzHandles[1] = new BuzzMoteAdapter(wfs,1);
      buzzHandles[2] = new BuzzMoteAdapter(wfs,2);
      buzzHandles[3] = new BuzzMoteAdapter(wfs,3);
      pollThread = new JInputPollThread(wfs, buzzController, buzzHandles);
      wfs.addRemoteAdapter(buzzHandles[0]);
      wfs.addRemoteAdapter(buzzHandles[1]);
      wfs.addRemoteAdapter(buzzHandles[2]);
      wfs.addRemoteAdapter(buzzHandles[3]);
    } else {
      wfs.log(CLASS_NAME + " Unable to find a wbuzz controller");

      // Collect keyboards.
      for (int i=0; i< ca.length; i++) {
        if (ca[i].getType() == Controller.Type.KEYBOARD) {
          kbControllers.add(ca[i]);
          wfs.log(CLASS_NAME + " Found keyboard controller named " + ca[i].getName());
          break;
        }
      }

      if (kbControllers.size() > 0) {
        BuzzMoteAdapter buzzHandles[] = new BuzzMoteAdapter[4];
        for (int i=0; i < 4; i++) {
          buzzHandles[i] = new BuzzMoteAdapter(wfs,i);
        }
        kbPollThread = new JInputKBPollThread(wfs, kbControllers, buzzHandles);
        for (int i=0; i < 4; i++) {
          wfs.addRemoteAdapter(buzzHandles[i]);
        }
      } else {
        // Somehow, didn't find any keyboards.
        wfs.log(CLASS_NAME + " Unable to find any keyboard controllers");
      }
    }

  }

  /**
    * Callback for WiiRemoteJ.findRemotes() to put found remotes into (only to be
    * used by real Wiimote discovery).
    * 
    * @param evt
    *          , WiiRemoteDiscoveredEvent
    */
  public void wiiRemoteDiscovered(WiiRemoteDiscoveredEvent evt) {
    wfs.addRemote(evt.getWiiRemote());
  }

  /**
    * 
    * @param numberFound
    *          , Amount of Wiiremotes found when searching
    */
  public void findFinished(int numberFound) {
    wfs.log(CLASS_NAME, "Search stopped, found " + numberFound + " Wiimote(s)");
  }

  public boolean isWiimoteDiscoveryEnabled() {
    // TODO: Replace this with actual config
    return false;
  }

  public boolean isBuzzJoystickDiscoveryEnabled() {
    // TODO: Replace this with actual config
    return true;
  }

  public boolean isKeyboardDiscoveryEnabled() {
    // TODO: Replace this with actual config
    return true;
  }
}
