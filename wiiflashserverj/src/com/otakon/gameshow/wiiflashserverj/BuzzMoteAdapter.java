package com.otakon.gameshow.wiiflashserverj;

import wiiremotej.WiiRemote;
import wiiremotej.WiiRemoteExtension;
import wiiremotej.event.WRButtonEvent;
import wiiremotej.event.WRStatusEvent;

import com.tojio.wiiflashserverj.WiiFlashServerJ;
import com.tojio.wiiflashserverj.WiiMoteAdapter;
import com.tojio.wiiflashserverj.WiiMoteData;

/**
 * Extension of WiiMoteAdapter to emulate the API but handle
 * 
 * @author jkao
 * 
 */
public class BuzzMoteAdapter extends WiiMoteAdapter {
  private final String CLASS_NAME="BuzzMoteAdapter";
  private int lampState;

  /**
   * This constructor should never be used because this class is not really a
   * WiiMote
   * 
   * @param wfs
   * @param remote
   * @param moteID
   */
  public BuzzMoteAdapter(WiiFlashServerJ wfs, WiiRemote remote, int moteID) {
    super(wfs, remote, moteID);

    throw new UnsupportedOperationException(
        "BuzzMoteAdapter doesn't support 3-arg constructor containing a WiiRemote object because it's not really a WiiMote.");
  }

  /**
   * Standard constructor
   * 
   * @param wfs
   * @param moteID
   *          Index for this buzz remote (0-3)
   */
  public BuzzMoteAdapter(WiiFlashServerJ wfs, int moteID) {
    super(wfs, null, moteID);

    // TODO: Fill in this.data (in superclass) info
    // Not sure yet what info is necessary, but some of it is definitely
    // required like id, icon
    // This data also shows up in the UI, so useful for status 

  }

  /**
   * Dummy per-remote initializer, Buzz controllers have no individual init
   */
  public boolean setup() {
    return true;
  }

  /**
   * Dummy WiiMoteData of Wiimote
   * 
   * @return all WiiMoteData
   */
  public WiiMoteData getData() {
    return data;
  }

  /**
   * Dummy LED control, Buzz controllers have no LEDs.
   * 
   * @param lightID
   *          (0=Off, 1=On, 2=Slow Flash, 3=Fast Flash)
   */
  public void setLEDLights(int lightID) {
    lampState = lightID;
  }
  
  public int getLEDLights() {
    return lampState;
  }

  /**
   * Dummy vibration control, Buzz controllers have no vibration
   */
  public void startVibrating() {
  }

  /**
   * Dummy vibration control, Buzz controllers have no vibration
   */
  public void stopVibrating() {
  }

  /**
   * Dummy mouse control, Buzz controllers have no accelerometer
   */
  public void startMouseControl() {
  }

  /**
   * Dummy mouse control, Buzz controllers have no accelerometer
   */
  public void stopMouseControl() {
  }

  /**
   * Dummy disconnect control, Buzz controllers can't be individually
   * disconnected
   */
  public void disconnect() {
  }

  /**
   * Primary callback to send button inputs. JInput poll thread should create a
   * WRCombinedEvent with the button data and feed it here.
   * 
   */
   /*
  public void combinedInputReceived(WRCombinedEvent evt) {
    super.combinedInputReceived(evt);
    */
  public void combinedInputReceived(int buttonData) {
    try {
      data.mButtons = 0;
      if ( (buttonData & WRButtonEvent.A) > 0) {
        data.mButtons += data.A;
      }
      if ( (buttonData & WRButtonEvent.B) > 0) {
        data.mButtons += data.B;
      }
      if ( (buttonData & WRButtonEvent.UP) > 0) {
        data.mButtons += data.UP;
      }
      if ( (buttonData & WRButtonEvent.DOWN) > 0) {
        data.mButtons += data.DOWN;
      }
      if ( (buttonData & WRButtonEvent.LEFT) > 0) {
        data.mButtons += data.LEFT;
      }
      if ( (buttonData & WRButtonEvent.RIGHT) > 0) {
        data.mButtons += data.RIGHT;
      }
      if ( (buttonData & WRButtonEvent.PLUS) > 0) {
        data.mButtons += data.PLUS;
      }
      if ( (buttonData & WRButtonEvent.MINUS) > 0) {
        data.mButtons += data.MINUS;
      }
      if ( (buttonData & WRButtonEvent.HOME) > 0) {
        data.mButtons += data.HOME;
      }
      if ( (buttonData & WRButtonEvent.ONE) > 0) {
        data.mButtons += data.ONE;
      }
      if ( (buttonData & WRButtonEvent.TWO) > 0) {
        data.mButtons += data.TWO;
      }
      wfs.sendRemoteData(data);
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Buzz controllers have no status, so this should never be called.
   */
  public void statusReported(WRStatusEvent evt) {
    super.statusReported(evt);
  }

  /**
   * Poll loop might call this if remote is disconnected.
   */
  public void disconnected() {
    wfs.log(CLASS_NAME + " " + data.id, "was disconnected");
    wfs.removeRemote(data.id);
  }

  /**
   * Dummy extension callback, Buzz controllers have no extensions
   */
  public void extensionConnected(WiiRemoteExtension extension) {
  }

  /**
   * Dummy extension callback, Buzz controllers have no extensions
   */
  public void extensionDisconnected(WiiRemoteExtension extension) {
  }

  /**
   * Dummy extension callback, Buzz controllers have no extensions
   */
  public void extensionPartiallyInserted() {
  }

  /**
   * Dummy extension callback, Buzz controllers have no extensions
   */
  public void extensionUnknown() {
  }

}
