/* 
 * Copyright (C) 2008 Alan Ross, Tojio GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/


package com.tojio.wiiflashserverj;

//import WiiremoteJ
import wiiremotej.event.*;


public class WiiMoteFinder implements WiiRemoteDiscoveryListener
{
	//used for log
	private final String CLASS_NAME = "WiiMoteFinder";
	private WiiFlashServerJ wfs;
	
	/**
	 * 
	 * @param wfs, the instance of WiiFlashServerJ
	 */
	public WiiMoteFinder(WiiFlashServerJ wfs)
    {
		super();
		this.wfs = wfs;
	}
	
	/**
	 * 
	 * @param evt, WiiRemoteDiscoveredEvent
	 */
	public void wiiRemoteDiscovered(WiiRemoteDiscoveredEvent evt)
    {
		wfs.addRemote(evt.getWiiRemote());
    }
    
	/**
	 * 
	 * @param numberFound, Amount of Wiiremotes found when searching
	 */
    public void findFinished(int numberFound)
    { 
    	wfs.log(CLASS_NAME,"Search stopped, found " + numberFound + " Wiimote(s)");
    }
    
    
    
}