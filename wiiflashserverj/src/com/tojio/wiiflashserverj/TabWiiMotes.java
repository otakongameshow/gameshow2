/* 
 * Copyright (C) 2008 Alan Ross, Tojio GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Note: WiiMoteAdapter.java has made extensive use of the demo-function-code 
 * (WRLImpl.java) provided by the WiiRemoteJ library by Michael Diamond.
*/


package com.tojio.wiiflashserverj;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.*;

public class TabWiiMotes extends JPanel
{
	public final static long serialVersionUID = 0;
	
	private ImageIcon ICON_CONNECTED;
	private ImageIcon ICON_NOTCONNECTED;
	private ImageIcon ICON_NUNCHUCK;
	private ImageIcon ICON_CLASSIC;
	
	private ArrayList<JLabel> icons;
	private ArrayList<JTextArea> infos;
	
	private JScrollPane sp;
	
	/**
	 * What a mess
	 */
	public TabWiiMotes()
	{
		JPanel p = new JPanel();
		p.setLayout(null);
		p.setPreferredSize(new Dimension(300, 250));
		p.setBackground(Color.WHITE);
		//p.setLayout(new GridLayout(4,2,0,10));
		//p.setBounds(0,0,100,100);
		//p.setOpaque(true);
		
		ICON_NOTCONNECTED = new ImageIcon(ClassLoader.getSystemResource("assets/wiimote_disconnected.png"));
		ICON_CONNECTED = new ImageIcon(ClassLoader.getSystemResource("assets/wiimote_connected.png"));
		ICON_NUNCHUCK = new ImageIcon(ClassLoader.getSystemResource("assets/wiimote_nunchuck.png"));
		ICON_CLASSIC = new ImageIcon(ClassLoader.getSystemResource("assets/wiimote_classic.png"));
		
		icons = new ArrayList<JLabel>();
		infos = new ArrayList<JTextArea>();
		
		Rectangle iconBounds = new Rectangle(20,15,50,50);
		Rectangle infoBounds = new Rectangle(100,15,300,50);
		
		for(int i = 0; i < 4; i++)
		{
			icons.add(makeIcon(ICON_NOTCONNECTED, iconBounds));
			iconBounds.y += 60;
			infos.add(makeInfo(infoBounds));
			infoBounds.y += 60;
			p.add(icons.get(i));
			p.add(infos.get(i));
		}
		
		sp = new JScrollPane(p,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		sp.setBorder(new CompoundBorder(new EmptyBorder(0,0,0,0) , new EtchedBorder()));
		
		setLayout(new GridLayout(1, 1, 0, 0));
		add(sp);
	}
	
	
	/**
	 * Creates a JLabel with given ImageIcon and its bounds
	 * 
	 * @param icon
	 * @param bounds
	 * @return JLabel
	 */
	private JLabel makeIcon(ImageIcon icon, Rectangle bounds)
	{
		JLabel l = new JLabel(icon);
		l.setBounds(bounds);
		return l;
	}
	
	
	/**
	 * Creates a JTextArea with given bounds
	 * 
	 * @param bounds
	 * @return JTextArea
	 */
	private JTextArea makeInfo(Rectangle bounds)
	{
		JTextArea t	= new JTextArea();
		t.setBounds(bounds);
		t.setFont(new Font("SansSerif", Font.PLAIN, 12));
		t.setBackground(null);
		t.setText(getInfo(null));
		return t;
	}
	
	
	/**
	 * Reset all Wiimote icons to disconnected status
	 */
	public void resetIcons()
	{
		for(int i = 0; i< infos.size(); i++)
		{
			icons.get(i).setIcon(ICON_NOTCONNECTED);
			infos.get(i).setText(getInfo(null));
		}
	}
	
	/**
	 * Reset given Wiimote icon to disconnected status
	 * 
	 * @param moteID, the id of the Wiimote
	 */
	public void resetIcon(int moteID)
	{
		for(int i = 0; i< infos.size(); i++)
		{
			if(moteID == i)
			{
				icons.get(i).setIcon(ICON_NOTCONNECTED);
				infos.get(i).setText(getInfo(null));
			}
		}
	}
	
	

	/**
	 * write a message to the log
	 */
	public void updateIcons(WiiMoteData data)
	{
		for(int i = 0; i< infos.size(); i++)
		{
			if(data != null && data.id == i)
			{
				if(data.hasExtension == 0) icons.get(i).setIcon(ICON_CONNECTED); //ohhh boy this is so stupid
				if(data.hasExtension == 1) icons.get(i).setIcon(ICON_NUNCHUCK);
				if(data.hasExtension == 2) icons.get(i).setIcon(ICON_CLASSIC);
				infos.get(i).setText(getInfo(data));
			}
		}
	}
	
	
	
	/**
	 * 
	 * @param data
	 * @return
	 */
	private String getInfo(WiiMoteData data)
	{
		String info = "";
		
		if(data != null)
		{
			info = "WiiMote ID: " + data.id + "\n";
			info += "Bluetooth ID: " + data.bluetooth + "\n";
			info += "Battery: " + (data.battery*100) + "%\n";
		}
		else
		{
			info = "WiiMote ID: - \n";
			info += "Bluetooth ID: - \n";
			info += "Battery: - \n";
		}
		
		return info;
	}
	

}
