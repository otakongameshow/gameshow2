#!/usr/bin/perl

use Test::More 'no_plan';
use Test::Differences;
use XML::XPath;
use XML::XPath::XMLParser;

my $inputFile = shift @ARGV;

print("Processing file $inputFile\n");
$xp = XML::XPath->new(filename=>$inputFile);

eq_or_diff(&xpCount("/gameshow/round"), 2, "num rounds");
eq_or_diff(&xpCount("/gameshow/final"), 1, "final");

for (my $i=1; $i<=2; $i++) {
  eq_or_diff(&xpCount("/gameshow/round[$i]/category"), 6, "categories in round $i");
  for (my $j=1; $j<=6; $j++) {
    eq_or_diff(&xpCount("/gameshow/round[$i]/category[$j]/name"), 1, "name for round $i, category $j");
    eq_or_diff(&xpCount("/gameshow/round[$i]/category[$j]/questions/question"), 5, "questions in round $i, category $j");
    for (my $k=1; $k<=5; $k++) {
      eq_or_diff(&xpCount("/gameshow/round[$i]/category[$j]/questions/question[$k]/prompt/text"), 1, "prompt for round $i, category $j, question $k");
      eq_or_diff(&xpCount("/gameshow/round[$i]/category[$j]/questions/question[$k]/answers/answer/text"), 5, "answers in round $i, category $j, question $k");
    }
  }
}

eq_or_diff(&xpCount("/gameshow/final/hint/text"), 1, "final hint");
eq_or_diff(&xpCount("/gameshow/final/prompt/text"), 1, "final prompt");
eq_or_diff(&xpCount("/gameshow/final/result/text"), 1, "final result");


sub xpCount {
  my $expression = shift;
  my $result = $xp->findvalue("count($expression)");
  return "$result";
}
