#!/usr/bin/env /var/www/apps/redmine/script/runner
#Product.find(:all).each { |p| p.price *= 2 ; p.save! }
#
# ruby gameshow-parser.rb <output xml>
#
require 'ftools'

@IMAGE_SRC='/var/www/localhost/htdocs/redmine/files/'
@IMAGE_DST='./images/'

QUESTION_TYPES = [
  :text,
  :free,
  :visual,
  :askaudience_text,
  :askaudience_visual
]

if !FileTest.directory?(@IMAGE_DST)
   puts "Create "+ @IMAGE_DST + " directory manually. Then run again."
	exit
end

class Answer
  attr_accessor :text, :correct, :media
  def initialize(atext, acorrect)
    self.text = atext
    self.correct = acorrect
  end
end

class Round
  attr_accessor :category_list, :category_count
  def initialize()
    @category_list = Array.new
    @category_count = 0
  end
end

class LCategory
  attr_accessor :question_list, :category
end

class LQuestion
  attr_accessor :correct_index, :answer_list, :question
end

class Final
  attr_accessor :hint_text, :hint_media
  attr_accessor :prompt_text, :prompt_media
  attr_accessor :result_text, :result_media
end


round = Array.new
round[1] = Round.new
round[2] = Round.new
final = Final.new

def place_answer (answer, index, ordered_list, random_list, correct)
  # if index is *, store answer in random_list
  # else store based on index in ordered_list (0 based)

  if index.chr == "*"
    puts "pushing random " + index.to_s
    random_list.push(Answer.new(answer,correct))
  else
    if index < 49 || index > 53
      throw "index was not 1-5 was " + index.chr
    else
      ordered_list[index-49] = Answer.new(answer,correct)
      puts "pushing ordered " + (index-49).to_s + " " + answer
    end
  end

end


@categories = Category.find_all_by_year_used(2014, :order=>"cat_order");
for category in @categories
  next if (category.cat_order < 11 || category.cat_order > 31 || category.cat_order % 10 > 6)
  puts "Processing " + category.name + " order " + category.cat_order.to_s
  round_number = category.cat_order / 10
  next if round_number == 0  #skip if round is blank
  if round_number == 3
	 @questions = category.questions(:order => "q_order")
	 for question in @questions
	   next if (question.q_order != 1)
		qrev = question.latest_revision
	 end
    #special final round handling
    #final.hint_text = row[col_category]
    #final.hint_media = row[col_order]
    #final.prompt_text = row[col_question]
    #final.prompt_media = row[col_qimage]
    #final.result_text = row[col_answer0]
    #final.result_media = row[col_aimage]
  end
  next if round_number == 3
  lcat = LCategory.new
  lcat.question_list = Array.new
  lcat.category = category
  c_name = category.name

  round[round_number].category_list.push(lcat)

  @questions = category.questions(:order => "q_order")
  for question in @questions
    qrev = question.latest_revision
    next if (question.q_order < 1 || question.q_order > 5)
    puts "Processing " + question.q_order.to_s + ": " 
	 lq = LQuestion.new
	 lq.question = question
    answer_list = Array.new
    random_answers = Array.new
    answer_order = qrev.answer_order
    if answer_order.nil? || answer_order.strip.empty?
      answer_order = "****"
    end
    #p row
	 answer = ""
	 wrong1 = ""
	 wrong2 = ""
	 wrong3 = ""
    if (QUESTION_TYPES[qrev.question_type] == :visual) || (QUESTION_TYPES[qrev.question_type] == :askaudience_text) || (QUESTION_TYPES[qrev.question_type] == :askaudience_visual)
	 unless qrev.answer_media.nil?
		File.copy(@IMAGE_SRC+qrev.answer_media.disk_filename,@IMAGE_DST)
      answer = qrev.answer_media.disk_filename
	 end
	 answer += "|" + qrev.answer
	 unless qrev.wrong1_media.nil?
		File.copy(@IMAGE_SRC+qrev.wrong1_media.disk_filename,@IMAGE_DST)
      wrong1 = qrev.wrong1_media.disk_filename
	 end
    wrong1 += "|" + qrev.wrong1
	 unless qrev.wrong2_media.nil?
		File.copy(@IMAGE_SRC+qrev.wrong2_media.disk_filename,@IMAGE_DST)
      wrong2 = qrev.wrong2_media.disk_filename
	 end
    wrong2 += "|" + qrev.wrong2
	 unless qrev.wrong3_media.nil?
		File.copy(@IMAGE_SRC+qrev.wrong3_media.disk_filename,@IMAGE_DST)
      wrong3 = qrev.wrong3_media.disk_filename
	 end
    wrong3 += "|" + qrev.wrong3
    else
      answer = qrev.answer
      wrong1 = qrev.wrong1
      wrong2 = qrev.wrong2
      wrong3 = qrev.wrong3
    end
    puts answer_order
    place_answer(answer,answer_order[0],answer_list,random_answers,true)
    place_answer(wrong1,answer_order[1],answer_list,random_answers,false)
    place_answer(wrong2,answer_order[2],answer_list,random_answers,false)
    place_answer(wrong3,answer_order[3],answer_list,random_answers,false)
    upper_limit = 3
    random_len = random_answers.length
    for i in 0..upper_limit
      if answer_list[i].nil?
        r = rand(random_len)
        answer_list[i] = random_answers[r]
        random_len -= 1
        random_answers[r] = random_answers[random_len]
        random_answers[random_len] = nil
      end
      if answer_list[i].correct
        lq.correct_index = i
      end
    end
    lq.answer_list = answer_list
    lcat.question_list.push(lq)
  end
end



puts "Parsing done. writing out"
oFile = File.new(ARGV[0], "w")

oFile.puts "<gameshow>"
for i in 1..2 do
  r = round[i]
  oFile.puts "<round>"
  r.category_list.each do |lc|
    c = lc.category
    oFile.puts " <category>"
    oFile.puts "  <name>" + c.name + "</name>"
    oFile.puts "  <questions>"
    lc.question_list.each do |lq|
	   q = lq.question
      qrev = q.latest_revision
      oFile.puts "   <question type=\"" + QUESTION_TYPES[qrev.question_type].to_s + "\""
      #oFile.puts "             preMedia=\"" + qrev.question_media.disk_filename + "\"" unless qrev.question_media.nil?
      oFile.puts "   >"
      oFile.puts "    <prompt>"
      oFile.puts "     <text>" + qrev.question_text.to_s + "</text>"
		if !qrev.question_media.nil?
		  File.copy(@IMAGE_SRC+qrev.question_media.disk_filename,@IMAGE_DST)
        oFile.puts "     <media>" + qrev.question_media.disk_filename + "</media>"
		end
      oFile.puts "    </prompt>"
      oFile.puts "    <answers correctIndex=\"" + lq.correct_index.to_s + "\">"
      lq.answer_list.each do |a|
        oFile.puts "     <answer>"
        if QUESTION_TYPES[qrev.question_type] == :visual || QUESTION_TYPES[qrev.question_type] == :askaudience_text || QUESTION_TYPES[qrev.question_type] == :askaudience_visual
          temp = a.text.to_s.split("|",2)
          oFile.puts "      <media>" + temp[0].to_s + "</media>"
          oFile.puts "      <text>" + temp[1].to_s + "</text>"
        else
          oFile.puts "      <text>" + a.text.to_s + "</text>"
        end
        oFile.puts "     </answer>"
      end
      oFile.puts "    </answers>"
      oFile.puts "    <result>"
      if qrev.post_media.nil?
        if qrev.answer_media.nil?
          result_media = nil
        else
          result_media = qrev.answer_media
        end
      else
        result_media = qrev.post_media
      end

		if !result_media.nil?
		  File.copy(@IMAGE_SRC+result_media.disk_filename,@IMAGE_DST)
        oFile.puts "     <media>" + result_media.disk_filename + "</media>"
		end
		if qrev.post_text.nil? || qrev.post_text.empty?
		   oFile.puts "      <text>" + qrev.answer.to_s + "</text>"
	   else
		   oFile.puts "      <text>" + qrev.post_text.to_s + "</text>"
	   end

      oFile.puts "    </result>"
      oFile.puts "   </question>"
    end
    oFile.puts "  </questions>"
    oFile.puts " </category>"
  end
  oFile.puts "</round>"
end
oFile.puts "<final>"
oFile.puts " <hint>"
oFile.puts "  <text>" + final.hint_text.to_s + "</text>"
oFile.puts "  <media>" + final.hint_media.to_s + "</media>"
oFile.puts " </hint>"
oFile.puts " <prompt>"
oFile.puts "  <text>" + final.prompt_text.to_s + "</text>"
oFile.puts "  <media>" + final.prompt_media.to_s + "</media>"
oFile.puts " </prompt>"
oFile.puts " <result>"
oFile.puts "  <text>" + final.result_text.to_s + "</text>"
oFile.puts "  <media>" + final.result_media.to_s + "</media>"
oFile.puts " </result>"
oFile.puts "</final>"
oFile.puts "</gameshow>"

oFile.close

#p "Round 1 found " + round[1].category_count.to_s + " categories."
#p "Round 2 found " + round[2].category_count.to_s + " categories."
