require "csv"


class Answer
	attr_accessor :text, :correct
	def initialize(atext, acorrect)
		self.text = atext
		self.correct = acorrect
	end
end

class Question
	attr_accessor :prompt, :prompt_media, :answer_list, :correct_index
	attr_accessor :result_media
end

class Category
	attr_accessor :name, :question_list, :order
	def initialize()
		@question_list = Array.new
	end
end

class Round
	attr_accessor :category_list, :category_count
	def initialize()
		@category_list = Array.new
		@category_count = 0
	end
end

class Final
	attr_accessor :hint_text, :hint_media
	attr_accessor :prompt_text, :prompt_media
	attr_accessor :result_text, :result_media
end


round = Array.new
round[1] = Round.new
round[2] = Round.new
final = Final.new
category_hash = Hash.new

def place_answer (answer, index, ordered_list, random_list, correct)
	# if index is *, store answer in random_list
	# else store based on index in ordered_list (0 based)

	if index.chr == "*"
		random_list.push(Answer.new(answer,correct))
	else
		if index < 49 || index > 53
			throw "index was not 1-5"
		else
			ordered_list[index-49] = Answer.new(answer,correct)
		end
	end

end


# row[0] round
# row[1] category
# row[2] question order - NaN rows are skipped (final round, media name)
# row[3] anime title - ignored
# row[4] question
# row[5] answer order - blank assumes *****
# row[6] correct answer
# row[7] wrong1
# row[8] wrong2
# row[9] wrong3
# row[10] wrong4
# row[11] image-Q
# row[12] image-A

CSV.open(ARGV[0],"r") do |row|
	round_number = row[0].to_i
  	next if round_number == 0  #skip if round is blank
	if round_number == 3
		#special final round handling
		final.hint_text = row[1]
		final.hint_media = row[2]
		final.prompt_text = row[4]
		final.prompt_media = row[11]
		final.result_text = row[6]
		final.result_media = row[12]
	end
	next if round_number == 3

	if category_hash.has_key?(row[0])
  	category = category_hash[row[0]]
	else
		category = Category.new
		category.name = row[1]
		category.order = round[round_number].category_count
		round[round_number].category_list.push(category)
		round[round_number].category_count += 1
		category_hash[row[0]] = category
	end

	answer_list = Array.new
	random_answers = Array.new
	question = Question.new
	question.prompt = row[5]
	question.prompt_media = row[12]
	question.result_media = row[13]
	answer_order = row[6]
	if answer_order.nil? || answer_order.strip.empty?
		answer_order = "*****"
	end

	answer_order = "12345"
	#p row
	place_answer(row[7],answer_order[0],answer_list,random_answers,true)
	place_answer(row[8],answer_order[1],answer_list,random_answers,false)
	place_answer(row[9],answer_order[2],answer_list,random_answers,false)
	place_answer(row[10],answer_order[3],answer_list,random_answers,false)
	place_answer(row[11],answer_order[4],answer_list,random_answers,false)

if false
	random_len = random_answers.length
	for i in 0..4
		if answer_list[i].nil?
			r = rand(random_len)
			answer_list[i] = random_answers[r]
			random_len -= 1
			random_answers[r] = random_answers[random_len]
			random_answers[random_len] = nil
		end
		if answer_list[i].correct
			question.correct_index = i
		end
	end
end
	question.answer_list = answer_list
	category.question_list.push(question)

end

oFile = File.new(ARGV[1], "w")

oFile.puts "<html>"
oFile.puts "<body>"
for i in 1..2 do
	r = round[i]
	oFile.puts "<h1> Round " + i.to_s + "</h1>"
	r.category_list.each do |c|
		oFile.puts "<h2>" + c.name + "</h2>"
		c.question_list.each do |q|
			oFile.puts "<h3>" + q.prompt + "</h3>"
			oFile.puts "<ul>"
			q.answer_list.each do |a|
				oFile.puts "<li>" + a.text.to_s + "</li>"
			end
			oFile.puts "</ul>"
		end
	end
end
oFile.puts "</body>"
oFile.puts "</html>"

oFile.close

p "Round 1 found " + round[1].category_count.to_s + " categories."
p "Round 2 found " + round[2].category_count.to_s + " categories."
