require "csv"

# ruby gameshow-parser.rb <input csv> <output xml>


class Answer
	attr_accessor :text, :correct, :media
	def initialize(atext, acorrect)
		self.text = atext
		self.correct = acorrect
	end
end

class Question
	attr_accessor :prompt, :prompt_media, :answer_list, :correct_index
	attr_accessor :result_media
	attr_accessor :question_type, :pre_media
end

class Category
	attr_accessor :name, :question_list, :order
	def initialize()
		@question_list = Array.new
	end
end

class Round
	attr_accessor :category_list, :category_count
	def initialize()
		@category_list = Array.new
		@category_count = 0
	end
end

class Final
	attr_accessor :hint_text, :hint_media
	attr_accessor :prompt_text, :prompt_media
	attr_accessor :result_text, :result_media
end


round = Array.new
round[1] = Round.new
round[2] = Round.new
final = Final.new
category_hash = Hash.new

def place_answer (answer, index, ordered_list, random_list, correct)
	# if index is *, store answer in random_list
	# else store based on index in ordered_list (0 based)

	if index.chr == "*"
		random_list.push(Answer.new(answer,correct))
	else
		if index < 49 || index > 53
			throw "index was not 1-5"
		else
			ordered_list[index-49] = Answer.new(answer,correct)
		end
	end

end

col_round = col = 0				# row[0] round
col_category = col += 1 		# row[1] category
col_order = col += 1				# row[2] question order - NaN rows are skipped (final round, media name)
col_type = col += 1				# row[3] question type (t, v, f) for text or visual or freeanswer
col_anime = col += 1				# row[4] anime title - ignored
col_question = col += 1			# row[5] question
col_answerorder = col += 1		# row[6] answer order - blank assumes *****
col_answer0 = col += 1			# row[7] correct answer
col_answer1 = col += 1			# row[8] wrong1
col_answer2 = col += 1			# row[9] wrong2
col_answer3 = col += 1			# row[10] wrong3
col_answer4 = col += 1			# row[11] wrong4
col_qimage = col += 1			# row[12] image-Q
col_aimage = col += 1			# row[13] image-A

CSV.open(ARGV[0],"r") do |row|
	round_number = row[col_round].to_i
  	next if round_number == 0  #skip if round is blank
	if round_number == 3
		#special final round handling
		final.hint_text = row[col_category]
		final.hint_media = row[col_order]
		final.prompt_text = row[col_question]
		final.prompt_media = row[col_qimage]
		final.result_text = row[col_answer0]
		final.result_media = row[col_aimage]
	end
	next if round_number == 3

	if category_hash.has_key?(row[col_round])
  	category = category_hash[row[col_round]]
	else
		category = Category.new
		category.name = row[col_category]
		category.order = round[round_number].category_count
		round[round_number].category_list.push(category)
		round[round_number].category_count += 1
		category_hash[row[col_round]] = category
	end

	next if row[col_order].to_i == 0
	answer_list = Array.new
	random_answers = Array.new
	question = Question.new
	question.prompt = row[col_question]
	question.prompt_media = row[col_qimage]
	question.result_media = row[col_aimage]
	question.pre_media = row[col_type].to_s.split("|",2)[1]
	character_type = row[col_type].downcase[0].chr
	if character_type == "v"
		question.question_type = :visual
	elsif character_type == "f"
		question.question_type = :free
	elsif character_type == "p"
		question.question_type = :price
	else
		question.question_type = :text
	end
	answer_order = row[col_answerorder]
	if answer_order.nil? || answer_order.strip.empty?
		answer_order = "*****"
	end

	#p row
	place_answer(row[col_answer0],answer_order[0],answer_list,random_answers,true)
	place_answer(row[col_answer1],answer_order[1],answer_list,random_answers,false)
	place_answer(row[col_answer2],answer_order[2],answer_list,random_answers,false)
	place_answer(row[col_answer3],answer_order[3],answer_list,random_answers,false)

	upper_limit = 3
	if question.question_type == :visual
		if row[col_answer4].nil? 
			upper_limit = 3
		else
			place_answer(row[col_answer4],answer_order[4],answer_list,random_answers,false)
		end
	else
		#place_answer(row[col_answer4],answer_order[4],answer_list,random_answers,false)
	end
	random_len = random_answers.length
	for i in 0..upper_limit
		if answer_list[i].nil?
			r = rand(random_len)
			answer_list[i] = random_answers[r]
			random_len -= 1
			random_answers[r] = random_answers[random_len]
			random_answers[random_len] = nil
		end
		if answer_list[i].correct
			question.correct_index = i
		end
	end
	question.answer_list = answer_list
	category.question_list.push(question)

end

oFile = File.new(ARGV[1], "w")

oFile.puts "<gameshow>"
for i in 1..2 do
	r = round[i]
	oFile.puts "<round>"
	r.category_list.each do |c|
		oFile.puts " <category>"
		oFile.puts "  <name>" + c.name + "</name>"
		oFile.puts "  <questions>"
		c.question_list.each do |q|
			oFile.puts "   <question type=\"" + q.question_type.to_s + "\""
			oFile.puts "             preMedia=\"" + q.pre_media + "\"" unless q.pre_media.nil?
			oFile.puts "   >"
			oFile.puts "    <prompt>"
			oFile.puts "     <text>" + q.prompt.to_s + "</text>"
			oFile.puts "     <media>" + q.prompt_media.to_s + "</media>"
			oFile.puts "    </prompt>"
			oFile.puts "    <answers correctIndex=\"" + q.correct_index.to_s + "\">"
			q.answer_list.each do |a|
				oFile.puts "     <answer>"
				if q.question_type == :visual
					temp = a.text.to_s.split("|",2)
					oFile.puts "      <media>" + temp[0].to_s + "</media>"
					oFile.puts "      <text>" + temp[1].to_s + "</text>"
				else
					oFile.puts "      <text>" + a.text.to_s + "</text>"
				end
				oFile.puts "     </answer>"
			end
			oFile.puts "    </answers>"
			oFile.puts "    <result>"
			oFile.puts "     <media>" + q.result_media.to_s + "</media>"
			oFile.puts "     <text>" + q.answer_list[q.correct_index].text + "</text>"
			oFile.puts "    </result>"
			oFile.puts "   </question>"
		end
		oFile.puts "  </questions>"
		oFile.puts " </category>"
	end
	oFile.puts "</round>"
end
if nil 
oFile.puts "<final>"
oFile.puts " <hint>"
oFile.puts "  <text>" + final.hint_text + "</text>"
oFile.puts "  <media>" + final.hint_media + "</media>"
oFile.puts " </hint>"
oFile.puts " <prompt>"
oFile.puts "  <text>" + final.prompt_text + "</text>"
oFile.puts "  <media>" + final.prompt_media + "</media>"
oFile.puts " </prompt>"
oFile.puts " <result>"
oFile.puts "  <text>" + final.result_text + "</text>"
oFile.puts "  <media>" + final.result_media + "</media>"
oFile.puts " </result>"
oFile.puts "</final>"
end
oFile.puts "</gameshow>"

oFile.close

p "Round 1 found " + round[1].category_count.to_s + " categories."
p "Round 2 found " + round[2].category_count.to_s + " categories."
