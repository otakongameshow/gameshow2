package gsComps {
    import flash.filesystem.File;
    import flash.filesystem.FileStream;
    
    public class MetricsLogger extends Object {

        private static var logFile:File = null;
        private static var logStream:FileStream = null;
        
        private static var currentQuestion:QuestionMetrics;

        public function MetricsLogger()
        {
            super();
        }

        public static function init(file:File):void {
            // Open logFile for writing (append)
            if (logStream != null) {
                close();
            }
            logFile = file;
            
            // Write header data.
            
        }
        
        public static function close():void {
            if (logStream != null) {
                // Write footer data (not used)

                // Close logfile
                logStream = null;
            }
        }
        
        public static function log(event:String, data:Array):void {
            if (logStream == null) {
                if (logFile != null) {
                    init(logFile);
                } else {
                    trace("Metrics log closed: " + event + " " + data);
                }
            } else {
                // Write event and timestamp
                // Then write data
                // flush stream.
            }
        }
        
        public static function logCurrentQuestion(complete:Boolean=true):void {
            if (currentQuestion != null) {
                log("Question",currentQuestion.toArray());
                currentQuestion = null;
            }
        }
        
        public static function startQuestion(qType:String, catIndex:int, qIndex:int):void {
            if (currentQuestion != null) {
                logCurrentQuestion(false);
            }
            currentQuestion = new QuestionMetrics();
            currentQuestion.questionType = qType;
            currentQuestion.categoryIndex = catIndex;
            currentQuestion.questionIndex = qIndex;
            currentQuestion.selectedTime = new Date();
        }
        
        public static function unlockQuestion():void {
            if (currentQuestion != null) {
                currentQuestion.unlockedTime = new Date();
            }
        }
        
        public static function lockQuestion(player:int):void {
            if (currentQuestion != null) {
                currentQuestion.lockedTime[player] = new Date();
            }
        }
        
        public static function markQuestion(player:int, correct:Boolean):void {
            if (currentQuestion != null) {
                currentQuestion.correctAnswer[player] = correct ? 1 : -1;
            }
        }
        
        public static function selectedAnswer(player:int):void {
            if (currentQuestion != null) {
                currentQuestion.timesChanged[player]++;
            }
        }

        public static function endQuestion():void {
            if (currentQuestion == null) {
                trace("endQuestion: currentQuestion was null");
            } else {
                currentQuestion.clearedTime = new Date();
                if (currentQuestion.unlockedTime == null) {
                    currentQuestion.unlockedTime = new Date();
                }
                for (var i:int=1; i<=4; i++) {
                    if (currentQuestion.lockedTime[i] == null) {
                        currentQuestion.lockedTime[i] = new Date();
                    }
                }
                logCurrentQuestion();
            }
        }

    }

}

