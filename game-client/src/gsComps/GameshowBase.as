package gsComps {
    import mx.core.Application;
    import flash.net.*;
    import flash.events.*;
    import mx.events.*;
    import mx.collections.ArrayCollection;
    import flash.geom.*;
    import org.wiiflash.Wiimote;
    import org.wiiflash.events.*;
    import flash.ui.Keyboard; // Keycode constants.
    import flash.display.StageDisplayState;
    import flash.filesystem.*; // AIR filesystem access
    import flash.system.Security;
    import mx.controls.Alert;
    import mx.controls.Label;
    import air.update.events.UpdateEvent; // Auto update 
    import air.update.ApplicationUpdaterUI; // Auto update 
    import flash.desktop.NativeApplication;
    

    public class GameshowBase extends Application {
        
        //Make a PlayerClass object to contain name, score, wiimote handles, etc.
        // Make players array 1 based.
        public var players:Array = new Array(5);
        

        // Gameshow state.
        public var gameshowFlow:XML;
        public var sceneIndex:int;
        public var sceneRewound:Boolean = false;
        public var currentScene:String = "";   //The current scene according to flow.xml. Loosely corresponds to currentState
        
        public var questions:XML;
        public var roundNumber:int;
        
        // Basing everything off of rootPath. 
        // Expecting media to be in rootPath/images.
        public var rootPath:File;
        public var mediaPath:File;
        
        // Gameshow components.
        [Bindable] public var my_Board:Board;
        [Bindable] public var my_ControllerTest:ControllerTest;
        [Bindable] public var my_NameEntry:NameEntry;
        [Bindable] public var my_SingleMedia:Media;
        [Bindable] public var my_FinalRound:FinalRound;
        [Bindable] public var my_FinalScore:FinalScore;
        [Bindable] public var my_AppVersion:Label;
        
        private var singleMediaIndex:int;
        
        private var filePath:String;
        
        public function GameshowBase()
        {
            super();
            
            // Initialize Players
            for (var i:int=1; i<=4; i++) {
                players[i] = new gsComps.Player(i, onWiiEvent);
            }
            // Temp initialization data
            //players[1].name="Allen";
            //players[2].name="Brian";
            //players[3].name="Christopher";
            //players[4].name="Doris";
            players[1].score=0;
            players[2].score=0;
            players[3].score=0;
            players[4].score=0;
            
            //Init preference defaults
            Prefs.init();
        }
        
        public function advanceScene():void {
            // Determine next scene, preload any data necessary and then change state.
            // Leaving scene never involves changing data.
            sceneIndex += 1;
            
            // If we've wrapped around, then reset sceneIndex to 0.
            if (sceneIndex >= gameshowFlow.scene.length()) {
                sceneIndex = 0;
                sceneRewound = false;
            }
            var nextScene:String = gameshowFlow.scene[sceneIndex].@state;

            currentScene = nextScene;
            currentState = currentScene;
            if (sceneRewound) {
                // If scene had been rewound, then just change state without reinitializing it.
                sceneRewound = false;
            } else {
                // If scene had not been rewound, then initialize according to scene and then changeState.
                trace(nextScene);
                switch (nextScene) {
                    case "Title":
                    break;
                    case "SingleMedia":
                    initializeSingleMedia();
                    break;
                    case "NameEntry":
                    break;
                    case "Round":
                    initializeRound();
                    break;
                    case "ControllerTest":
                    initializeControllerTest();
                    break;
                    case "FinalRound":
                    initializeFinalRound();
                    break;
                    case "FinalScore":
                    break;
                    default:
                    trace("Unknown scene value encountered = " + nextScene);
                    // Remaining in same state.
                    //nextScene = currentState;
                    break;
                }
            }
        }

        public function rewindScene(): void {
            // Safety in case of accidental advancement of scene.
            // Just changeState back to previous scene and do not reinitialize any thing.
            // We'll only allow the rewind of one scene for now because of how we handle scene initialization.
            if (!sceneRewound && sceneIndex > 0) {
                sceneIndex -= 1;
                sceneRewound = true;
                currentScene = gameshowFlow.scene[sceneIndex].@state;
                currentState = currentScene;
            } else {
                // If scene was previously rewound or we're already at the top, then
                // don't do anything.
            }
        }
        
        public function updateScores(): void {
            var i:int;
            for(i=1;i<=4;i++) {
                my_Board["my_Scoreboard"]["p"+i].value = players[i].score;
            }
        }

        private function initializeSingleMedia():void {
            singleMediaIndex = 0;
            loadSingleMediaSlide();
        }
        
        private function loadSingleMediaSlide():void {
            if (singleMediaIndex < 0) {
                singleMediaIndex = 0;
            } else if (singleMediaIndex >= gameshowFlow.scene[sceneIndex].file.length()) {
                singleMediaIndex = gameshowFlow.scene[sceneIndex].file.length()-1;
            } else {
                my_SingleMedia.load(gameshowFlow.scene[sceneIndex].file[singleMediaIndex].@value);
                my_SingleMedia.togglePlayPause();
            }
        }
        
        private function handleSingleMediaKey(event:KeyboardEvent):void {
            switch (event.keyCode) {
                case Keyboard.SPACE:
                if (event.shiftKey) {
                    singleMediaIndex--;
                } else {
                    singleMediaIndex++;
                }
                my_SingleMedia.stop();
                loadSingleMediaSlide();
                break;
                case Keyboard.RIGHTBRACKET:
                if (event.shiftKey) {
                    my_SingleMedia.rewind();
                } else {
                    my_SingleMedia.togglePlayPause();
                }
                break;
            }
        }

        public function initializeRound(): void {
            roundNumber = gameshowFlow.scene[sceneIndex].@value;
            trace("roundNumber = "+ roundNumber)
            if (roundNumber < 1 || roundNumber > 2) {
                trace("roundNumber out of bounds. Forcing to 1");
                roundNumber = 1;
            }
            
            // Send round data to Board and let it worry about the rest.
            my_Board.initializeRound(roundNumber, players, questions.round[roundNumber-1]);
        }

        private function initializeControllerTest():void {
            my_ControllerTest.reset();
            for (var i:int=1;i<=4;i++) {
                my_ControllerTest["label"+i].text = players[i].name;
            }
        }

        public function initializeFinalRound(): void {
            my_FinalRound.initializeRound(questions.final[0]);
        }
        
        private function updateFinalScore():void {
            // Bubblesort player scores.
            var sorted:Array = new Array(5);
            var maxPlayer:int;
            var temp:Player;
            var i:int;
            var j:int;
            
            for (i=1;i<=4;i++) {
                sorted[i] = players[i];
            }
            for (i=1;i<=4;i++) {
                maxPlayer=i;
                for (j=i+1;j<=4;j++) {
                    if (sorted[maxPlayer].score < sorted[j].score) {
                        maxPlayer = j;
                    }
                }
                temp = sorted[i];
                sorted[i] = sorted[maxPlayer];
                sorted[maxPlayer] = temp;
                my_FinalScore["name"+i].text = sorted[i].name;
                my_FinalScore["score"+i].text = sorted[i].score;
                if (sorted[1].score == sorted[i].score) {
                    my_FinalScore["name"+i].setStyle("fontSize","40");
                    my_FinalScore["score"+i].setStyle("fontSize","40");
                } else {
                    my_FinalScore["name"+i].setStyle("fontSize","30");
                    my_FinalScore["score"+i].setStyle("fontSize","30");
                }
            }
        }

        public function onWiiEvent(event:MPButtonEvent):void {
            //trace("onWiiEvent("+event.id+") "+event);
            switch(currentScene) {
                case "Round":
                my_Board.onWiiEvent(event);
                break;
                case "ControllerTest":
                my_ControllerTest.onWiiEvent(event);
                break;
            }
        }
        
        public function onKeyDown(event:KeyboardEvent):void {
            // Do scene changing related stuff and then forward 
            //trace(event);
            if (event.keyCode == Keyboard.BACKSLASH) {
                if (event.shiftKey) {
                    this["my_AudienceMiniPrize"].toggleDisplay();
                } else {
                    this["my_AudienceScoreboard"].toggleDisplay();
                }
            } else if (this["my_AudienceMiniPrize"].visible) {
                // Space bar to start selection.
                if (event.keyCode == Keyboard.SPACE) {
                    this["my_AudienceMiniPrize"]["winnerName"].visible=true;
                    this["my_AudienceMiniPrize"]["correctNames"].visible=false;
                }
            } else if (this["my_AudienceScoreboard"].visible) {
                // Block anything from being processed on the lower level
            } else if (event.keyCode == 39 && event.shiftKey) {
                advanceScene();
            } else if (event.keyCode == 37 && event.shiftKey) {
                rewindScene();
            } else if (event.keyCode == Keyboard.F && event.controlKey) {
                stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
            } else {
                switch(currentScene) {
                    case "Title":
                    // Title (currently) has no events or handling.
                    break;
                    case "SingleMedia":
                    handleSingleMediaKey(event);
                    break;
                    case "Round":
                    //trace("Sending event to my_Board");
                    my_Board.onKeyDown(event);
                    break;
                    case "ControllerTest":
                    my_ControllerTest.onKeyDown(event);
                    break;
                    case "FinalRound":
                    my_FinalRound.onKeyDown(event);
                    break;
                    default:
                    break;
                }
            }
        }

        public function onStateChanging(event:StateChangeEvent):void {
            
        }
        
        public function onStateChanged(event:StateChangeEvent):void {
            var i:int;
            
            trace(event);
            trace("currentTarget " + event.currentTarget + " target " + event.target + " oldState " + event.oldState + " newState " + event.newState);
            if (stage != null) {
                stage.focus = stage;
            }
            switch(event.oldState) {
                case "NameEntry":
                // Get playerdata.
                for (i=1; i<=4; i++) {
                    players[i].name = my_NameEntry["p"+i+"Name"].text;
                }
                break;
                case "SingleMedia":
                my_SingleMedia.stop();
                break;
            }
            switch(event.newState) {
                case "Title":
                for (i=1; i<=4; i++) {
                    players[i].setLamp(2);
                }
                break;
                case "NameEntry":
                for (i=1; i<=4; i++) {
                    my_NameEntry["p"+i+"Name"].text = players[i].name;
                }
                break;
                case "Round":
                for (i=1; i<=4; i++) {
                    players[i].setLamp(0);
                }
                //updateScores();
                break;
                case "FinalScore":
                updateFinalScore();
                break;
            }
        }
        
        private function findFlow():Boolean {
            // Look for flow.xml and prompt if not found.
            // If found in applicationDirectory or applicationDirectory/../data then devel mode.
            // In devel mode, rootPath is where flow.xml is, mediaPath is ../../flash/images
            // Otherwise, look in documentsDirectory/gameshow.
            // Otherwise, prompt.

            var appPath:File;
            var testPath:File;
            
            appPath = new File(File.applicationDirectory.nativePath);
            testPath = appPath.resolvePath("flow.xml");
            trace(testPath.nativePath);
            
            if (testPath.exists) {
                rootPath = appPath;
                mediaPath = appPath.resolvePath("../../flash/images");
                return true;
            }
            
            testPath = appPath.resolvePath("../data/flow.xml");
            trace(testPath.nativePath);
            
            if (testPath.exists) {
                rootPath = appPath.resolvePath("../data");
                mediaPath = appPath.resolvePath("../../flash/images");
                return true;
            }
            
            testPath = File.documentsDirectory.resolvePath("gameshow/flow.xml");
            trace(testPath.nativePath);
            
            if (testPath.exists) {
                rootPath = File.documentsDirectory.resolvePath("gameshow");
                mediaPath = rootPath.resolvePath("images");
                return true;
            }
            
            var directory:File = File.documentsDirectory;
            directory.addEventListener(Event.SELECT, onDirectorySelected);
            directory.browseForDirectory("Select Directory");

            // Prompt for gameshow directory.
            return false;
        }
        
        private function onDirectorySelected(event:Event):void {
            var testPath:File;
            testPath = event.target.resolvePath("flow.xml");
            trace(testPath.nativePath);
            if (testPath.exists) {
                rootPath = event.target as File;
                mediaPath = rootPath.resolvePath("images");
                readConfig();
            } else {
                File.documentsDirectory.browseForDirectory("Select Directory");
            }
        }
        
        private function readConfig():void {
            // Reading XML files via AIR - http://help.adobe.com/en_US/AIR/1.5/devappsflex/WS5b3ccc516d4fbf351e63e3d118666ade46-7dc5.html
            var file:File;
            var fileStream:FileStream = new FileStream(); 

            file = rootPath.resolvePath("flow.xml");
            trace("Looking for flow.xml at " + file.nativePath);
            fileStream.open(file, FileMode.READ); 
            gameshowFlow = XML(fileStream.readUTFBytes(fileStream.bytesAvailable)); 
            fileStream.close();
            
            if (gameshowFlow.prefs != undefined) {
                Prefs.setFromConfig(gameshowFlow.prefs);
            }

            file = rootPath.resolvePath("questions.xml");
            trace("Looking for questions.xml at " + file.nativePath);
            fileStream.open(file, FileMode.READ);
            questions = XML(fileStream.readUTFBytes(fileStream.bytesAvailable));
            fileStream.close();
            
            Assets.loadSounds();
            
			sceneIndex = -1;
			advanceScene();
        }
        
        private var appUpdater:ApplicationUpdaterUI = new ApplicationUpdaterUI();
        private function checkUpdate():void {
            setApplicationVersion();
            // we set the URL for the update.xml file
            appUpdater.updateURL = "http://www.cjas.org/redmine/dist/update.xml";
            //we set the event handlers for INITIALIZED nad ERROR
            appUpdater.addEventListener(UpdateEvent.INITIALIZED, onUpdate);
            appUpdater.addEventListener(ErrorEvent.ERROR, onError);
            //we can hide the dialog asking for permission for checking for a new update;
            //if you want to see it just leave the default value (or set true).
            appUpdater.isCheckForUpdateVisible = false;
            //we initialize the updater
            appUpdater.initialize();
        }
        private function setApplicationVersion():void {
            var appXML:XML = NativeApplication.nativeApplication.applicationDescriptor;
            var ns:Namespace = appXML.namespace();
            my_AppVersion.text = "App version: " + appXML.ns::version;
        }
        private function onUpdate(event:UpdateEvent):void {
            //start the process of checking for a new update and to install
            appUpdater.checkNow();
        }
        private function onError(event:ErrorEvent):void {
            Alert.show(event.toString());
        }

		public function onCreationComplete(event:FlexEvent):void {
        	checkUpdate();
		}
		
		public function onAddedToStage(event:Event):void {
		    trace(event);
			//this.stage.scaleMode = StageScaleMode.SHOW_ALL;
			this.stage.fullScreenSourceRect = new Rectangle(0,0,1024,768);
//			this.stage.displayState = "fullScreenInteractive";
            //stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
            this.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyDown);
            if (findFlow()) {
                readConfig();
            }
            SMS.init(this["my_NetStatus"]);
		}
        
    }

}

