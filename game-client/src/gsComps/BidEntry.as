package gsComps {
    import mx.controls.TextInput;
    import flash.events.TextEvent;

    public class BidEntry extends TextInput {

        public function BidEntry()
        {
            super();
            this.addEventListener(TextEvent.TEXT_INPUT, onTextInput);
        }

        private function onTextInput(event:TextEvent):void {
            if (event.text == "-") {
                event.preventDefault();
                if (event.target.text == "") {
                    event.target.text="-";
                    event.target.setSelection(event.target.text.length,event.target.text.length);
                    event.target.setStyle("color","red");
                } else {
                    var i:int = event.target.text;
                    i = -i;
                    event.target.text=i;
                    event.target.setSelection(event.target.text.length,event.target.text.length);
                    event.target.setStyle("color",(i < 0) ?"red" : "green")
                }
            }
        }



    }

}

