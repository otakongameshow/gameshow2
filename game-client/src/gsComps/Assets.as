package gsComps {

public final class Assets extends Object {
	
	import flash.media.Sound;
    import flash.media.SoundChannel;
    import flash.media.SoundTransform;
    import flash.filesystem.File;
	import mx.core.Application;
	import flash.net.URLRequest;
	
	[Embed(source="/assets/icon-fa.svg")] public static const FA_ICON:Class;
	[Embed(source="/assets/icon-mc.svg")] public static const MC_ICON:Class;
	[Embed(source="/assets/badge-correct.svg")] public static const CORRECT_BADGE:Class;
	[Embed(source="/assets/badge-wrong.svg")] public static const WRONG_BADGE:Class;
	[Embed(source="/assets/badge-arrow.svg")] public static const ARROW_BADGE:Class;
	[Embed(source="/assets/badge-queue.svg")] public static const QUEUE_BADGE:Class;
	[Embed(source="/assets/badge-select.svg")] public static const SELECT_BADGE:Class;
	[Embed(source="/assets/badge-lock.svg")] public static const LOCK_BADGE:Class;
	[Embed(source="/assets/badge-1.svg")] public static const C1_BADGE:Class;
	[Embed(source="/assets/badge-2.svg")] public static const C2_BADGE:Class;
	[Embed(source="/assets/badge-3.svg")] public static const C3_BADGE:Class;
	[Embed(source="/assets/badge-4.svg")] public static const C4_BADGE:Class;
	[Embed(source="/assets/background-gradient1.svg")] public static const GRAD1_BG:Class;
	[Embed(source="/assets/background-gradient1-wide.svg")] public static const GRAD1W_BG:Class;
	[Embed(source="/assets/background-gradient2.svg")] public static const GRAD2_BG:Class;
	
	[Embed(source="/assets/soundonly1.jpg")] public static const AUDIO_IMAGE:Class;
	
	public static const BLUE_COLOR:int = 0x0000ff;
	public static const ORANGE_COLOR:int = 0xff7f00;
	public static const GREEN_COLOR:int = 0x00ff00;
	public static const YELLOW_COLOR:int = 0xffff00;
	public static const DEFAULT_COLOR:int = 0xafafaf;
	public static const MC_COLORS:Array = [DEFAULT_COLOR,BLUE_COLOR,ORANGE_COLOR,GREEN_COLOR,YELLOW_COLOR,DEFAULT_COLOR];
	public static const MC_BADGES:Array = [SELECT_BADGE,C1_BADGE,C2_BADGE,C3_BADGE,C4_BADGE,SELECT_BADGE];
		
	private static var correctSounds:Array = new Array();
	private static var correctPlay:Array = new Array();
	private static var wrongSounds:Array = new Array();
	private static var wrongPlay:Array = new Array();
	
	private static var faBuzzInSound:Sound = null;
	private static var mcSelectSound:Sound = null;
	private static var mcLockInSound:Sound = null;
	private static var scoreUpdateSound:Sound = null;
	private static var timerTickSounds:Array = new Array();
	private static var timerTickSound:Sound = null;
    
	
	public function Assets()
	{
		super();
	}
	
	public static function loadSound(file:File):Sound {
	    if (file.exists) {
	        return new Sound((new URLRequest(file.url)));
	    } else {
	        trace ("Unable to find sound: " + file.nativePath);
	        return null;
	    }
	}
	public static function loadSounds():void {
	    var soundDir:File = Application.application.mediaPath.resolvePath("../sfx");
		var fileList:Array = soundDir.getDirectoryListing();
		var index:int = 0;
		var length:int = fileList.length;
		var testName:String;
		
        // Correct/Wrong sound loading.
        for (index = 0; index < length; index++) {
            testName=soundDir.getRelativePath(fileList[index]);
            if (testName.toLowerCase().search("^right-") == 0) {
                correctSounds.push(new Sound(new URLRequest(fileList[index].url)));
                //trace("c:"+fileList[index].url)
            } else if (testName.toLowerCase().search("^wrong-") == 0) {
                wrongSounds.push(new Sound(new URLRequest(fileList[index].url)));
                //trace("w:"+fileList[index].url + ":" + wrongSounds.length);
            } else if (testName.toLowerCase().search("^timertick-") == 0) {
                timerTickSounds.push(new Sound(new URLRequest(fileList[index].url)));
            }
        }
        
        for (index = 0; index < correctSounds.length; index++) {
            correctPlay[index] = correctSounds[index];
        }
        for (index = 0; index < wrongSounds.length; index++) {
            wrongPlay[index] = wrongSounds[index];
        }
        
        // BuzzIn sound loading.
        faBuzzInSound = loadSound(soundDir.resolvePath("buzzsound.mp3"));
        mcSelectSound = loadSound(soundDir.resolvePath("mcselect.mp3"));
        mcLockInSound = loadSound(soundDir.resolvePath("mclockinsound.mp3"));
        scoreUpdateSound = loadSound(soundDir.resolvePath("scoreupdate.mp3"));
        //timerTickSound = loadSound(soundDir.resolvePath("timertick.mp3"));
        queueRandomTimerTick();
		
	}
	
	private static function playSoundList(playList:Array, soundList:Array):void {
	    var index:int = Math.floor(Math.random()*playList.length);
	    var ac:SoundChannel;
	    // Check that any sounds were loaded at all. Do nothing if none were.
	    if (soundList.length > 0) {
            ac = playList[index].play();
	        setVolume(ac, .3);
            playList.splice(index,1);
            if (playList.length == 0) {
                for (index = 0; index < soundList.length; index++) {
                    playList[index] = soundList[index];
                }
            }
        }
	}
	
	public static function queueRandomTimerTick():void {
	    var index:int = Math.floor(Math.random()*timerTickSounds.length);
	    timerTickSound = timerTickSounds[index];
	}
	
	public static function setVolume(ac:SoundChannel, volume:Number):void {
	    var transform:SoundTransform;
	    transform = ac.soundTransform;
	    transform.volume = volume;
	    ac.soundTransform = transform;
	}
	
	public static function playFABuzzIn():void {
	    var ac:SoundChannel;
	    if (faBuzzInSound != null) {
	        ac = faBuzzInSound.play();
	        setVolume(ac, .3);
	    }
	}
	public static function playMCSelect():void {
	    var ac:SoundChannel;
	    if (mcSelectSound != null) {
	        ac = mcSelectSound.play();
	        setVolume(ac, .3);
	    }
	}
	public static function playMCLockIn():void {
	    var ac:SoundChannel;
	    if (mcLockInSound != null) {
	        ac = mcLockInSound.play();
	        setVolume(ac, .3);
	    }
	}
	public static function playScoreUpdate():void {
	    var ac:SoundChannel;
	    if (scoreUpdateSound != null) {
	        ac = scoreUpdateSound.play();
	        setVolume(ac, .3);
	    }
	}
	public static function playTimerTick():void {
	    var ac:SoundChannel;
	    if (timerTickSound != null) {
	        ac = timerTickSound.play();
	        setVolume(ac, .3);
	    }
	}
	
	public static function playCorrect():void {
	    playSoundList(correctPlay, correctSounds);
	}

	public static function playWrong():void {
	    playSoundList(wrongPlay, wrongSounds);
	}
}

}

