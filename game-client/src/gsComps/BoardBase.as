package gsComps {
    import mx.core.Application;

    import mx.containers.Canvas;
    import mx.controls.Image;
    import flash.events.*;
    import flash.ui.Keyboard;
    import mx.events.FlexEvent;
    import org.wiiflash.events.ButtonEvent;
    import flash.filesystem.*; // AIR filesystem access
    import flash.utils.ByteArray;
    import mx.events.StateChangeEvent;
    import mx.effects.Sequence;
    import mx.events.EffectEvent;
    import mx.controls.Text; // For font resize


    public class BoardBase extends Canvas {

        public var selectedCategory:int = -1;
        // questionsLeft is 1-based.
        public var questionsLeft:Array = new Array(7);
        private var currentQuestion:XML;
        private var currentCorrectAnswer:int;
        private var questionValue:int;          // Points for question.
        private var currentReveal:int = 0;      // Iterator through question scenes.
        private var questionMarked:Boolean;     // has the question been marked correct?
        private var questionScored:Boolean;     // has the question been scored correct?
        private var questionTimeUp:Boolean;     // has the timer expired?
        private var questionDone:Boolean;       // is question done with all phases and can be safely disposed?
        private var playerPoints:Array = new Array(5); // points for the player for this question
        private var scoringIndex:int;           // who in buzzOrder is being marked.
        private var answersShown:Boolean;       // For MC, if player answers were shown.
        private var controlIndex:int;           // which player has control of board.
        private var audienceMode:Boolean;       // True for ask audience, false for normal mode.
        private var audienceAnswer:int;
        private var audienceAnswerEntered:Boolean;
        private var clearingQuestion:Boolean;

        [Bindable] public var my_category1:Category;
        [Bindable] public var my_category2:Category;
        [Bindable] public var my_category3:Category;
        [Bindable] public var my_category4:Category;
        [Bindable] public var my_category5:Category;
        [Bindable] public var my_category6:Category;

        [Bindable] public var my_FASingleMedia:FASingleMedia;
        [Bindable] public var my_MCSingleMedia:MCSingleMedia;
        [Bindable] public var my_MCMultiImage:MCMultiImage;
        [Bindable] public var my_Scoreboard:Scoreboard;
        [Bindable] public var my_Timer:TimerDisplay;
        [Bindable] public var my_DropQuestion:Sequence;
        private var playableMedia:Media;
        
        private var preMediaCache:String; // state of question type that preMedia is preempting.

        [Bindable] public var roundData:XML;
        [Bindable] public var roundNumber:int;
        
        private var buzzedIn:Array = new Array(5);
        private var buzzOrder:Array = new Array(5);
        private var buzzChoice:Array = new Array(5);
        private var buzzCount:int;

        public function BoardBase() {
            super();
        }

        public function initializeRound(myRoundNumber:int, players:Array, myRoundData:XML):void {
            var i:int;
            var j:int;
            var category:Category;

            roundNumber = myRoundNumber;
            roundData = myRoundData;
            
            currentState = null;

            selectedCategory=-1;

            for (i=1;i<=6;i++) {
                // Reset questionsLeft;
                questionsLeft[i] = 5;
            }
            for (i=1;i<=4;i++) {
                this["my_Scoreboard"]["p"+i].assignPlayer(players[i]);
            }
            
            my_Scoreboard.unrecline();

            // Revisibilize question status (and later set question type probably)
            for (i=1;i<=6;i++) {
                category = this["my_category"+i]
                if (category != null) {
                    this["my_category"+i].alpha=1.0;
                    this["my_category"+i].x=0;
                    category["remaining"].x=510;
                    category.text = roundData.category[i-1].name;
                    setCategoryBorder(i,"none");
                    for (j=1;j<=5;j++) {
                        //trace ("roundData:" + roundData.category[i-1].questions.question.length());
                        if (roundData.category[i-1].questions.question.length() >= j) {
                            var image:CategoryBadge = this["my_category"+i]["question"+j];
                            if (image != null) {
                                image.visible=true;
                                image.value = String(100 * roundNumber *j);
                                if (roundData.category[i-1].questions.question[j-1].@type == "free") {
                                    image.source=Assets.FA_ICON;
                                } else {
                                    image.source=Assets.MC_ICON;
                                }
                            }
                        }
                    }
                }
            }
          }

          private function writePreview(data:String):void {
              if (Prefs.previewEnabled) {
                  if (Prefs.previewMethod == "local") {
                      var file:File = new File(Prefs.previewPath);
                      var stream:FileStream = new FileStream();

                      stream.open(file, FileMode.WRITE);
                      stream.writeMultiByte(data, "us-ascii");
                      stream.close();
                  }
              }
          }

        public function initializeQuestion(categoryIndex:int):void {
            // Setup questionIndex to be 1 based.
            var questionIndex:int = 6-questionsLeft[categoryIndex];
            questionsLeft[categoryIndex]--;

            currentQuestion = roundData.category[categoryIndex-1].questions.question[questionIndex-1];
            currentCorrectAnswer = currentQuestion.answers.@correctIndex;
            questionMarked = false;
            questionScored = false;
            questionTimeUp = false;
            questionDone = false;
            for (var i:int = 1; i<=4; i++) {
                playerPoints[i] = 0;
            }
            scoringIndex = 1;
            answersShown = false;
            questionValue = questionIndex * 100 * roundNumber;
            //trace(currentQuestion);

            writePreview("Waiting for network.");

            playableMedia = null;
            
            my_Scoreboard.hide();
            this["my_HideCategory"].play();

            // randomize timertick sound
            Assets.queueRandomTimerTick();

            // Hide a question icon
            var image:CategoryBadge = this["my_category"+categoryIndex]["question"+questionIndex];
            image.visible=false;
            this["my_category"+categoryIndex].removeBadge.play();
            // If no more questions left, hide the category.
            if (questionsLeft[categoryIndex] == 0) {
                this["my_category"+categoryIndex].alpha=0.1;
            }
            

            // Check for PreMedia.
            var preMedia:String = currentQuestion.@preMedia;
            if (preMedia != "") {
                initializePreMedia();
            }
            

            // Switch to appropriate question handling state based on type.
            var type:String = currentQuestion.@type;

            MetricsLogger.startQuestion(type,categoryIndex,questionIndex);
            
            audienceAnswer = -1;
            audienceAnswerEntered = false;
            clearingQuestion = false;
            switch (type) {
                case "free": {
                    audienceMode = false;
                    initializeFASingleMedia();
                    break;
                }
                case "text": {
                    SMS.sendQuestionBegin(roundNumber,categoryIndex-1,questionIndex-1,currentCorrectAnswer, false);
                    audienceMode = false;
                    initializeMCSingleMedia();
                    break;
                }
                case "visual": {
                    SMS.sendQuestionBegin(roundNumber,categoryIndex-1,questionIndex-1,currentCorrectAnswer, false);
                    audienceMode = false;
                    initializeMCMultiImage();
                    break;
                }
                case "askaudience_text": {
                    SMS.sendQuestionBegin(roundNumber,categoryIndex-1,questionIndex-1,currentCorrectAnswer, true);
                    audienceMode = true;
                    initializeMCSingleMedia();
                    break;
                }
                case "askaudience_visual": {
                    SMS.sendQuestionBegin(roundNumber,categoryIndex-1,questionIndex-1,currentCorrectAnswer, true);
                    audienceMode = true;
                    initializeMCMultiImage();
                    break;
                }
                default:
                trace("Unknown question type = " + type);
                break;
            }
        }
        
        private function initializePreMedia():void {
            var media:Media;
            
            media = this["PreMedia"];
            media.load(currentQuestion.@preMedia);
            media.setVolume(currentQuestion.@preMediaVolume);
            currentState = "PreMedia";
            media.togglePlayPause();
        }

        private function initializeFASingleMedia():void {
            var i:int;

            for (i=1;i<=4;i++) {
                buzzOrder[i] = 0;
                buzzChoice[i] = 0;
            }
            lockAllBuzzers(false);
            // Component states
            currentReveal = 0;

            if (currentState == "PreMedia") {
                preMediaCache = "FASingleMedia";
            } else {
                currentState = "FASingleMedia";
            }

            var faBox:FASingleMedia = my_FASingleMedia;

            faBox.currentState = null;

            faBox["reveal1"].visible=false;
            faBox["reveal2"].visible=false;

            faBox["reveal1"].load(currentQuestion.prompt.media);
            faBox["reveal1"].setVolume(currentQuestion.prompt.media.@volume);
            faBox["reveal1"].rotationY=0;
            playableMedia = faBox["reveal1"]
            faBox.r2Data=currentQuestion.prompt.text;
            faBox["answer1"].load(currentQuestion.result.media);
            faBox["answer1"].setVolume(currentQuestion.result.media.@volume);
            faBox.a2Data=currentQuestion.prompt.text;
            if (currentQuestion.result.text == null) {
              faBox.a3Data=currentQuestion.answers.answer[currentCorrectAnswer].text;
            } else {
              faBox.a3Data=currentQuestion.result.text;
            }
            
            MetricsLogger.unlockQuestion();

        }

        private function initializeMCSingleMedia():void {
            var i:int;

            // Buzz variables
            for (i=1;i<=4;i++) {
                buzzOrder[i] = 0;
                buzzChoice[i] = 0;
            }
            lockAllBuzzers(true);

            if (currentState == "PreMedia") {
                preMediaCache = "MCSingleMedia";
            } else {
                currentState = "MCSingleMedia";
            }
        
            // Component states
            currentReveal = 0;

            var mcBox:MCSingleMedia = my_MCSingleMedia;

            mcBox.currentState = null;
            mcBox.glowingIndex = -1;

            mcBox["reveal1"].visible=false;
            mcBox["reveal2"].visible=false;
            mcBox["reveal3"].visible=false;
            mcBox["reveal4"].visible=false;
            mcBox["reveal5"].visible=false;
            mcBox["reveal6"].visible=false;

            mcBox["reveal1"].load(currentQuestion.prompt.media);
            mcBox["reveal1"].setVolume(currentQuestion.prompt.media.@volume);
            mcBox["reveal1"].rotationY=0;
            playableMedia = mcBox["reveal1"]
            mcBox.r2Data=currentQuestion.prompt.text;
            mcBox["reveal2b"].setStyle("fontSize",29);
            mcBox.r3Data="1: "+currentQuestion.answers.answer[0].text;
            mcBox.r4Data="2: "+currentQuestion.answers.answer[1].text;
            mcBox.r5Data="3: "+currentQuestion.answers.answer[2].text;
            mcBox.r6Data="4: "+currentQuestion.answers.answer[3].text;
            mcBox["answer1"].load(currentQuestion.result.media);
            mcBox["answer1"].setVolume(currentQuestion.result.media.@volume);
            mcBox.a2Data=currentQuestion.prompt.text;
            updateAnswerMCSingleMedia();
        }

        private function updateAnswerMCSingleMedia():void {
            var mcBox:MCSingleMedia = my_MCSingleMedia;
            var i:int;
            i = currentCorrectAnswer;
            i++;
            if (currentQuestion.result.text == null || audienceMode) {
              mcBox.a3Data=String(i) + ": " + currentQuestion.answers.answer[i-1].text;
            } else {
              mcBox.a3Data=String(i) + ": " + currentQuestion.result.text;
            }
            mcBox["answer3"].setStyle("borderColor", Assets.MC_COLORS[i]);
        }

        private function initializeMCMultiImage():void {
            var i:int;

            //var qData0:ByteArray = loadBinaryFile(currentQuestion.prompt.media);
            var qData1:ByteArray = loadBinaryFile(currentQuestion.answers.answer[0].media);
            var qData2:ByteArray = loadBinaryFile(currentQuestion.answers.answer[1].media);
            var qData3:ByteArray = loadBinaryFile(currentQuestion.answers.answer[2].media);
            var qData4:ByteArray = loadBinaryFile(currentQuestion.answers.answer[3].media);

            for (i=1;i<=4;i++) {
                buzzOrder[i] = 0;
                buzzChoice[i] = 0;
            }
            lockAllBuzzers(true);

            if (currentState == "PreMedia") {
                preMediaCache = "MCMultiImage";
            } else {
                currentState = "MCMultiImage";
            }
            
            // Component states
            currentReveal = 0;

            var mc2Box:MCMultiImage = my_MCMultiImage;

            mc2Box.currentState = null;
            mc2Box.glowingIndex = -1;
            
            mc2Box["reveal1"].visible=false;
            mc2Box["reveal1a"].visible=false;
            mc2Box["reveal2"].visible=false;
            mc2Box["reveal3"].visible=false;
            mc2Box["reveal4"].visible=false;
            mc2Box["reveal5"].visible=false;
            mc2Box["reveal2b"].visible=false;
            mc2Box["reveal3b"].visible=false;
            mc2Box["reveal4b"].visible=false;
            mc2Box["reveal5b"].visible=false;

            mc2Box.r1Data=currentQuestion.prompt.text;
            mc2Box["reveal1a"].load(currentQuestion.prompt.media);
            mc2Box["reveal1a"].setVolume(currentQuestion.prompt.media.@volume);
            mc2Box["reveal1a"].rotationY=0;
            playableMedia = mc2Box["reveal1a"]
            //mc2Box.r1Image=qData0;
            mc2Box.r2Image=qData1;
            mc2Box.r3Image=qData2;
            mc2Box.r4Image=qData3;
            mc2Box.r5Image=qData4;
            mc2Box.r2Data="1: "+currentQuestion.answers.answer[0].text;
            mc2Box.r3Data="2: "+currentQuestion.answers.answer[1].text;
            mc2Box.r4Data="3: "+currentQuestion.answers.answer[2].text;
            mc2Box.r5Data="4: "+currentQuestion.answers.answer[3].text;
            mc2Box["answer1"].load(currentQuestion.result.media);
            mc2Box["answer1"].setVolume(currentQuestion.result.media.@volume);
            mc2Box.a2Data=currentQuestion.prompt.text;
            updateAnswerMCMultiImage();
        }

        private function updateAnswerMCMultiImage():void {
            var mc2Box:MCMultiImage = my_MCMultiImage;
            var i:int;
            i = currentCorrectAnswer;
            i++;
            if (currentQuestion.result.text == null || audienceMode) {
              mc2Box.a3Data=String(i) + ": " + currentQuestion.answers.answer[i-1].text;
            } else {
              mc2Box.a3Data=String(i) + ": " + currentQuestion.result.text;
            }
            mc2Box["answer3"].setStyle("borderColor", Assets.MC_COLORS[i]);
        }


        private function loadBinaryFile(name:String):ByteArray {

            try {
                var data:ByteArray = new ByteArray();

                var mediaStream:FileStream = new FileStream(); 

                mediaStream.open(Application.application.mediaPath.resolvePath("./"+name), FileMode.READ);
                mediaStream.readBytes(data,0,mediaStream.bytesAvailable);
                mediaStream.close();

                return data;
            } catch (error:Error) {
                return null;
            }
            return null;
        }

        private function setCategoryBorder(index:int, style:String):void {
            if (index < 0 || index > 6) {
                return;
            }
            var category:Category = this["my_category"+index];
            switch (style) {
                case "none":
                category.unglow();
                break;
                case "solid":
                category.glow();
                break;
            }
        }

        private function selectCategory(index:int):void {
            //trace("selectCategory("+index+")");
            // If no more questions left in category, return
            if (questionsLeft[index]==0) {
                return;
            }
            // If index != selectedCategory then reflect on the view.
            // If index == selectedCategory then transition state to question.
            if (index != selectedCategory) {
                setCategoryBorder(selectedCategory, "none");
                selectedCategory = index;
                setCategoryBorder(selectedCategory, "solid");
            } else {
                // Unselect category
                setCategoryBorder(selectedCategory, "none");
                var questionCategory:int = selectedCategory;
                selectedCategory = -1;
                // Start question
                initializeQuestion(questionCategory);
            }
        }

        private function increaseScore(player:int, value:int):void {
            Application.application.players[player].score += value;
            //Application.application.updateScores();
            Assets.playScoreUpdate();
            // Add code for score animation.
        }

        private function lockAllBuzzers(state:Boolean):void {
            // A flashing buzzer should generally indicate an unlocked buzzer (player should be prepared to buzz in).
            // An unlit buzzer should indicate a locked buzzer.
            for (var i:int=1; i<= 4; i++) {
                buzzedIn[i] = state;
                if (state) {
                    Application.application.players[i].setLamp(0);
                } else {
                    Application.application.players[i].setLamp(2);
                    buzzCount = 0;
                    scoringIndex = 1;
                }
            }
        }

        private function relightRemainingBuzzers():void {
            // If player hasn't buzzed in, then make his buzzer flash again.
            for (var i:int=1; i<=4; i++) {
                if (!buzzedIn[i]) {
                    Application.application.players[i].setLamp(2);
                }
            }
        }


        private function markCorrect():void {
            if (!questionMarked && buzzCount >= scoringIndex) {
                questionMarked = true;
                // Lock remaining buzzers.
                lockAllBuzzers(true);
                // Show correct banner.
                //showBanner(0,false);
                Assets.playCorrect();
                // Show correct badge.
                showCorrectBadge(buzzOrder[scoringIndex]);
                MetricsLogger.markQuestion(buzzOrder[scoringIndex],true);
                playerPoints[buzzOrder[scoringIndex]] = questionValue;
                // Set control of board;
                controlIndex = buzzOrder[scoringIndex];
            }
        }

        private function markWrong():void {
            if (!questionMarked && buzzCount >= scoringIndex) {
                //showBanner(-1,false);
                Assets.playWrong();
                // Show wrong badge.
                showWrongBadge(buzzOrder[scoringIndex]);
                MetricsLogger.markQuestion(buzzOrder[scoringIndex],false);
                // Unlight lamp.
                Application.application.players[buzzOrder[scoringIndex]].setLamp(0);

                // Increase scoring index.
                scoringIndex++;
                // If everyone's been scored, then lock question.
                if (scoringIndex > 4) {
                    questionMarked = true;
                }
                // If people waiting on queue, then showBanner for next player after delay
                if (scoringIndex <= buzzCount) {
                    showArrowBadge(buzzOrder[scoringIndex]);
                    //showBanner(buzzOrder[scoringIndex],false);
                    //Light this guy's lamp
                    Application.application.players[buzzOrder[scoringIndex]].setLamp(1);
                } else {
                    // If queue has been emptied, relight remaining buzzers.
                    relightRemainingBuzzers();
                }
            }
        }

        private function revealMCAnswers():void {
            if (!answersShown) {
                lockAllBuzzers(true);
                answersShown=true;
                // For now dump in label.

                var output:String;
                output = "";
                for (var i:int=1;i<=4;i++) {
                    showChoiceBadge(i);
                    //output += Application.application.players[i].name + ": " + buzzChoice[i] + " / ";
                }
                //this["my_"+currentState]["playerAnswers"].text = output;
            }
        }
        
        private function markMC():void {
            var i:int;

            if (!questionMarked) {
                questionMarked = true;
                for (i=buzzCount; i>=1; i--) {
                    var correctAnswer:int = currentCorrectAnswer;
                    if (buzzChoice[buzzOrder[i]] == (correctAnswer+1)) {
                        //Show correctBadge
                        showCorrectBadge(buzzOrder[i]);
                        MetricsLogger.markQuestion(buzzOrder[i],true);
                        playerPoints[buzzOrder[i]] = questionValue;
                        // Set controlIndex;
                        controlIndex = buzzOrder[i];
                    } else {
                        //Show wrong badge
                        showWrongBadge(buzzOrder[i]);
                        MetricsLogger.markQuestion(buzzOrder[i],false);
                    }
                }
            } else {
                trace("markMC: questionScore is true");
            }
        }
        
        private function scoreMC():void {
            var i:int;

            if (!questionScored) {
                questionScored = true;
                for (i=1; i<=4; i++) {
                    if (playerPoints[i] > 0) {
                        showPointsBadge(i,playerPoints[i]);
                    }
                }
            }
        }

        private function showArrowBadge(i:int):void {
            my_Scoreboard["p"+i].changeBadge("image",Assets.ARROW_BADGE);
        }
        private function showChoiceBadge(i:int):void {
            this["my_Scoreboard"]["p"+i].changeBadge("image",buzzChoice[i] ? Assets.MC_BADGES[buzzChoice[i]] : "" ,Assets.MC_COLORS[buzzChoice[i]]);
        }
        private function showCorrectBadge(i:int):void {
            this["my_Scoreboard"]["p"+i].changeBadge("image",Assets.CORRECT_BADGE);
        }
        private function showWrongBadge(i:int):void {
            this["my_Scoreboard"]["p"+i].changeBadge("image",Assets.WRONG_BADGE);
        }
        private function showPointsBadge(i:int, points:int):void {
            this["my_Scoreboard"]["p"+i].changeBadge("text",points);
        }
        private function showOrderingBadge(i:int,order:int):void {
            this["my_Scoreboard"]["p"+i].showBadge("image",Assets.QUEUE_BADGE);
        }
        private function showSelectBadge(i:int):void {
            this["my_Scoreboard"]["p"+i].changeBadge("image",Assets.SELECT_BADGE);
            MetricsLogger.selectedAnswer(i);
        }
        private function showLockedBadge(i:int):void {
            this["my_Scoreboard"]["p"+i].changeBadge("image",Assets.LOCK_BADGE);
        }
        private function hideBadges():void {
            for (var i:int=1;i<=4;i++) {
                this["my_Scoreboard"]["p"+i].hideBadge();
            }
        }

        public function onWiiEvent(event:MPButtonEvent):void {
            switch (currentState) {
                case "FASingleMedia":
                handleFASingleMediaBuzz(event);
                break;
                case "MCSingleMedia":
                case "MCMultiImage":
                // MC states have the same behavior for buzz.
                handleMCSingleMediaBuzz(event);
                break;
            }
        }

        private function handleFASingleMediaBuzz(event:MPButtonEvent):void {
            // Accept any button as a buzz-in.
            // If player's already buzzed in, then ignore any further events.
            if (!buzzedIn[event.id]) {
                buzzedIn[event.id] = true;
                buzzCount++;
                buzzOrder[buzzCount] = event.id;
                trace("FA("+event.id+") buzzed in " + buzzCount);

                MetricsLogger.lockQuestion(event.id);
                // Pause media.
                playableMedia.stop();
                my_FASingleMedia.recline();
                my_Scoreboard.show();
                // For first player, prompt to answer (activate lamp, show banner).
                if (buzzCount == scoringIndex) {
                    for (var i:int=1; i<=4; i++) {
                        Application.application.players[i].setLamp(0);
                    }
                    Application.application.players[event.id].setLamp(1);
                    //showBanner(event.id,true);
                    showArrowBadge(event.id);
                    Assets.playFABuzzIn();
                } else {
                    // Activate player's badge with buzz-in placement.
                    showOrderingBadge(event.id,buzzCount);
                }
            }
        }

        private function lockInChoice(id:int, force:Boolean):void {
            // If player has registered a choice, then lock in.
            if (!buzzedIn[id]) {
                if (buzzChoice[id] != 0 || force) {
                    buzzedIn[id] = true;
                    buzzCount++;
                    buzzOrder[buzzCount] = id;
                    Application.application.players[id].setLamp(1);
                    showLockedBadge(id);
                    Assets.playMCLockIn();
                    trace("MC("+id+") locked in " + buzzChoice[id]);
                    MetricsLogger.lockQuestion(id);
                }
            }
        }
        private function handleMCSingleMediaBuzz(event:MPButtonEvent):void {
            // If player's locked in (using buzzedIn), then ignore
            if (!buzzedIn[event.id]) {
                switch (event.type) {
                    case ButtonEvent.A_PRESS:
                    lockInChoice(event.id,false);
                    break;
                    case ButtonEvent.UP_PRESS:
                    trace("MC("+id+") selected 1");
                    buzzChoice[event.id] = 1;
                    Application.application.players[event.id].setLamp(3);
                    showSelectBadge(event.id);
                    Assets.playMCSelect();
                    break;
                    case ButtonEvent.RIGHT_PRESS:
                    buzzChoice[event.id] = 2;
                    Application.application.players[event.id].setLamp(3);
                    showSelectBadge(event.id);
                    Assets.playMCSelect();
                    break;
                    case ButtonEvent.DOWN_PRESS:
                    buzzChoice[event.id] = 3;
                    Application.application.players[event.id].setLamp(3);
                    showSelectBadge(event.id);
                    Assets.playMCSelect();
                    break;
                    case ButtonEvent.LEFT_PRESS:
                    buzzChoice[event.id] = 4;
                    Application.application.players[event.id].setLamp(3);
                    showSelectBadge(event.id);
                    Assets.playMCSelect();
                    break;
                }
            }
        }
        
        public function onKeyDown(event:KeyboardEvent):void {
            if (event.keyCode == Keyboard.N && event.shiftKey) {
                if (currentState == "MCSingleMedia" || currentState == "MCMultiImage") {
                    SMS.resendQuestionBegin();
                }
            } else if (event.keyCode == Keyboard.M && event.shiftKey) {
                if (currentState == null) {
                    SMS.resendQuestionEnd();
                }
            }
            switch (currentState) {
                case null:
                handleBaseKey(event);
                break;
                case "FASingleMedia":
                handleFASingleMediaKey(event);
                break;
                case "MCSingleMedia": 
                handleMCSingleMediaKey(event);
                break;
                case "MCMultiImage":
                handleMCMultiImageKey(event);
                break;
                case "PreMedia":
                handlePreMediaKey(event);
                break;
            }
        }
        
        private function handleBaseKey(event:KeyboardEvent):void {
            // F1-F6 for category selection
            // ,. for score adjustment
            // TBD for AudienceScoreboard display
            switch (event.keyCode) {
                case Keyboard.F1:
                selectCategory(1);
                break;
                case Keyboard.F2:
                selectCategory(2);
                break;
                case Keyboard.F3:
                selectCategory(3);
                break;
                case Keyboard.F4:
                selectCategory(4);
                break;
                case Keyboard.F5:
                selectCategory(5);
                break;
                case Keyboard.F6:
                selectCategory(6);
                break;
                case Keyboard.TAB:
                if (controlIndex > 0) {
                    my_Scoreboard["p"+controlIndex].unglow();
                }
                controlIndex += event.shiftKey ? -1 : 1;
                controlIndex = (controlIndex % 5 + 5) % 5;
                if (controlIndex > 0) {
                    my_Scoreboard["p"+controlIndex].glow();
                }
                break;
                case Keyboard.COMMA:
                if (controlIndex > 0) {
                    Application.application.players[controlIndex].score -= event.shiftKey ? 10 : 100;
                }
                break;
                case Keyboard.PERIOD:
                if (controlIndex > 0) {
                    Application.application.players[controlIndex].score += event.shiftKey ? 10 : 100;
                }
                break;
                case Keyboard.SPACE:                
                break;
            }
       }
        
       private function resizeField(text:Text, limit:int):void {
           var i:int = int(text.getStyle("fontSize"));
           if (text.height > limit) {
               trace("textheight = " + text.height);
               i--;
               text.setStyle("fontSize",i);
               this.callLater(resizeField,[text,limit]);
           }
           
       }

        private function handleFASingleMediaKey(event:KeyboardEvent):void {
            switch(event.keyCode) {
                case Keyboard.SPACE: {
                    currentReveal++;
                    if (currentReveal == 1) {
                        writePreview(currentQuestion.prompt.text);
                    }
                    resizeField(my_FASingleMedia["reveal2b"], 74);
                    if (currentReveal <= 2) {
                        this["my_FASingleMedia"]["reveal"+currentReveal].visible = true;
                    }
                    break;
                }
                case Keyboard.ENTER:
                if (my_FASingleMedia.currentState != "Answer") {
                    if (questionMarked || event.shiftKey) {
                        my_FASingleMedia.currentState = "Answer";
                        lockAllBuzzers(true);
                        playableMedia.stop();
                        playableMedia = my_FASingleMedia["answer1"];
                    }
                } else {
                    if (!questionScored) {
                        scoreMC();
                    } else {
                        hideBadges();
                        questionDone = true;
                    }
                }
                break;
                case Keyboard.RIGHTBRACKET:
                if (event.shiftKey) {
                    playableMedia.rewind();
                } else {
                    playableMedia.togglePlayPause();
                }
                break;
                
                case Keyboard.I: { 
                    // Correct
                    markCorrect();
                }

                break;
                case Keyboard.O: { 
                    // Wrong
                    markWrong();
                }
                break;
                case Keyboard.P:
                if (questionDone || event.shiftKey) {
                    if (!clearingQuestion) {
                        clearingQuestion = true;
                        lockAllBuzzers(true);
                        MetricsLogger.endQuestion();
                        playableMedia.stop();
                        hideBadges();
                        my_DropQuestion.play();
                    }
                }
                break;
            }
        }
        
        private function glowAudienceAnswer(box:Object, answerIndex:int):void {
            for (var i:int = 0; i < 4; i++) {
                if (box.glowingIndex == i && i != answerIndex) {
                    box["unglow"+i].play();
                }
                if (answerIndex == i && box.glowingIndex != i) {
                    box["glow"+i].play();
                }
            }
            box.glowingIndex = answerIndex;
        }
        
        private function processAudienceAnswer(box:Object, answerIndex:int):void {
            if (audienceMode && box.currentState == null && !answersShown) {
                currentCorrectAnswer = answerIndex;
                audienceAnswerEntered = true;
                audienceAnswer = answerIndex;
                // Show some kind of visual confirmation.
                glowAudienceAnswer(box, answerIndex);
            }
        }
        
        private function handleMCSingleMediaKey(event:KeyboardEvent):void {
            switch(event.keyCode) {
                case Keyboard.SPACE:
                currentReveal++;
                resizeField(my_MCSingleMedia["reveal2b"], 74);
                if (currentReveal == 1) {
                    writePreview(currentQuestion.prompt.text);
                }
                if (currentReveal <= 6) {
                    this["my_MCSingleMedia"]["reveal"+currentReveal].visible = true;
                    if (currentReveal > 2) {
                        writePreview(this["my_MCSingleMedia"]["reveal"+currentReveal+"b"].text);
                    }
                }
                if (currentReveal == 7) {
                    my_MCSingleMedia.recline.play();
                    my_Scoreboard.show();
                    my_Timer.startTimer(handleTimeUp);
                    lockAllBuzzers(false);
                    MetricsLogger.unlockQuestion();
                }
                break;
                case Keyboard.RIGHTBRACKET:
                if (event.shiftKey) {
                    playableMedia.rewind();
                } else {
                    playableMedia.togglePlayPause();
                }
                break;
                case Keyboard.ENTER:
                if (my_MCSingleMedia.currentState == null) {
                    if (!audienceMode || audienceMode && audienceAnswerEntered) {
                        if (questionTimeUp || event.shiftKey) {
                            if (!answersShown) {
                                playableMedia.stop();
                                handleTimeUp();
                                revealMCAnswers(); 
                                if (audienceMode) {
                                    updateAnswerMCSingleMedia();
                                }
                            } else {
                                my_MCSingleMedia.currentState = "Answer";
                                if (audienceMode) {
                                    var testLoad:ByteArray;
                                    testLoad = loadBinaryFile(currentQuestion.answers.answer[audienceAnswer].media);
                                    if (testLoad) {
                                        trace("loaded audience answer");
                                        my_MCSingleMedia["answer1"].load(currentQuestion.answers.answer[audienceAnswer].media);
                                    }
                                }
                                playableMedia = my_MCSingleMedia["answer1"];
                            }
                        }
                    }
                } else {
                    if (!questionMarked) {
                        markMC();
                    } else if (!questionScored) {
                        scoreMC();
                    } else {
                        hideBadges();
                        questionDone = true;
                    }
                }
                break;
                case Keyboard.I:
                processAudienceAnswer(my_MCSingleMedia, 0);
                break;
                case Keyboard.J:
                processAudienceAnswer(my_MCSingleMedia, 1);
                break;
                case Keyboard.K:
                processAudienceAnswer(my_MCSingleMedia, 2);
                break;
                case Keyboard.L:
                processAudienceAnswer(my_MCSingleMedia, 3);
                break;
                case Keyboard.P:
                if (questionDone || event.shiftKey) {
                    if (!clearingQuestion) {
                        clearingQuestion = true;
                        lockAllBuzzers(true);
                        MetricsLogger.endQuestion();
                        playableMedia.stop();
                        hideBadges();
                        my_Timer.stopTimer();
                        my_DropQuestion.play();
                        glowAudienceAnswer(my_MCSingleMedia, -1);
                        SMS.sendQuestionEnd(audienceAnswer, audienceMode);
                    }
                }
                break;
            }
        }

        private function handleMCMultiImageKey(event:KeyboardEvent):void {
            switch(event.keyCode) {
                case Keyboard.SPACE:
                if (currentReveal == 0) {
                    currentReveal++;
                    writePreview(currentQuestion.prompt.text);
                    my_MCMultiImage["unfade1"].play();
                    my_MCMultiImage["reveal1a"].visible = true;
                } else if (currentReveal == 1) {
                    resizeField(my_MCMultiImage["reveal1b"], 74);
                    my_MCMultiImage["reveal"+currentReveal].visible = true;
                    currentReveal++;
                } else if (currentReveal <= 5) {
                    if (currentReveal > 1) {
                        writePreview(my_MCMultiImage["reveal"+currentReveal+"c"].text);
                    }
                        if (my_MCMultiImage["reveal"+currentReveal].visible) {
                            my_MCMultiImage["zoom"+currentReveal].resume();
                            my_MCMultiImage["reveal"+currentReveal+"b"].visible = true;
                            currentReveal++;
                        } else {
                            if (currentReveal == 2) {
                                my_MCMultiImage["fade1"].play();
                            }
                            my_MCMultiImage["zoom"+currentReveal].play();
                            my_MCMultiImage["zoom"+currentReveal].pause();
                            my_MCMultiImage["reveal"+currentReveal].visible = true;
                        }
                } else if (currentReveal == 6) {
                    currentReveal++;
                    my_Scoreboard.show();
                    my_Timer.startTimer(handleTimeUp);
                    lockAllBuzzers(false);
                    MetricsLogger.unlockQuestion();
                }
                break;
                case Keyboard.RIGHTBRACKET:
                if (playableMedia != null) {
                    if (event.shiftKey) {
                        playableMedia.rewind();
                    } else {
                        playableMedia.togglePlayPause();
                    }
                }
                break;
                case Keyboard.ENTER:
                if (my_MCMultiImage.currentState == null) {
                    if (!audienceMode || audienceMode && audienceAnswerEntered) {
                        if (questionTimeUp || event.shiftKey) {
                            if (!answersShown) {
                                playableMedia.stop();
                                handleTimeUp();
                                revealMCAnswers(); 
                                if (audienceMode) {
                                    updateAnswerMCMultiImage();
                                }
                            } else {
                                my_MCMultiImage.currentState = "Answer";
                                if (audienceMode) {
                                    my_MCMultiImage["answer1"].load(currentQuestion.answers.answer[audienceAnswer].media);
                                }
                                playableMedia = my_MCMultiImage["answer1"];
                            }
                        }
                    }
                } else {
                    if (!questionMarked) {
                        markMC();
                    } else if (!questionScored) {
                        scoreMC();
                    } else {
                        hideBadges();
                        questionDone = true;
                    }
                }
                break;
                case Keyboard.I:
                processAudienceAnswer(my_MCMultiImage, 0);
                break;
                case Keyboard.J:
                processAudienceAnswer(my_MCMultiImage, 1);
                break;
                case Keyboard.K:
                processAudienceAnswer(my_MCMultiImage, 2);
                break;
                case Keyboard.L:
                processAudienceAnswer(my_MCMultiImage, 3);
                break;
                case Keyboard.P:
                if (questionDone || event.shiftKey) {
                    if (!clearingQuestion) {
                        clearingQuestion = true;
                        lockAllBuzzers(true);
                        MetricsLogger.endQuestion();
                        hideBadges();
                        if (playableMedia != null) {
                            playableMedia.stop();
                        }
                        my_Timer.stopTimer();
                        my_DropQuestion.play();
                        glowAudienceAnswer(my_MCMultiImage, -1);
                        SMS.sendQuestionEnd(audienceAnswer, audienceMode);
                    }
                }
                break;
            }
        }

        private function handlePreMediaKey(event:KeyboardEvent):void {
            switch (event.keyCode) {
                case Keyboard.SPACE:
                this["PreMedia"].stop();
                currentState = preMediaCache;
                break;
            }
        }        
        private function handleTimeUp():void {
            for (var i:int=1;i<=4;i++) {
                lockInChoice(i,true);
            }
            questionTimeUp = true;
        }

        public function onCurrentStateChange(event:StateChangeEvent):void {
            var i:int;
            
            switch(event.oldState) {
                case null:
                case "":
                if (controlIndex > 0) {
                    my_Scoreboard["p"+controlIndex].unglow();
                }
                break;
            }
            switch(event.newState) {
                case null:
                case "":
                if (controlIndex > 0) {
                    my_Scoreboard["p"+controlIndex].glow();
                }
                for (i=1;i<=6;i++) {
                    if (questionsLeft[i] > 0) {
                        this["my_category"+i].nextValue= ": " + (roundNumber * 100 * (6-questionsLeft[i]));
                    } else {
                        this["my_category"+i].nextValue= "";
                    }
                }
                // saveGameState();
                break;
            }
        }
        
        public function onDropQuestionEnd(event:EffectEvent):void {
            currentState = null;
            this["my_ShowCategory"].play();
            my_Scoreboard.show();
            my_Scoreboard.unrecline();
        }
        
        public function onCreationComplete(event:FlexEvent):void {
            // Nothing to do right now.
            //trace ("Board " + event);
            my_DropQuestion.addEventListener(EffectEvent.EFFECT_END,onDropQuestionEnd);
        }
    }

}


