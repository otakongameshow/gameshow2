package gsComps {

    public class QuestionMetrics extends Object {
        public var questionType:String;
        public var categoryIndex:int;
        public var questionIndex:int;
        public var selectedTime:Date;
        public var unlockedTime:Date;
        public var lockedTime:Array;
        public var correctAnswer:Array;
        public var timesChanged:Array;
        public var clearedTime:Date;
        
        public function QuestionMetrics() {
            super();
            lockedTime = new Array(5);
            correctAnswer = new Array(5);
            timesChanged = new Array(5);
            for (var i:int=1; i<=4; i++) {
                correctAnswer[i] = 0;
                timesChanged[i] = 0;
            }
        }
        
        public function toArray():Array {
            return [questionType, categoryIndex, questionIndex, selectedTime.time, unlockedTime.time,
            lockedTime[1].time, lockedTime[2].time, lockedTime[3].time, lockedTime[4].time, 
            correctAnswer[1], correctAnswer[2], correctAnswer[3], correctAnswer[4],
            timesChanged[1], timesChanged[2], timesChanged[3], timesChanged[4], 
            clearedTime.time];
        }
    }
}

