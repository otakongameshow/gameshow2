package gsComps {

	import flash.events.Event;
    import mx.controls.Label;

	import flash.net.URLVariables;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.sendToURL;
	import flash.events.IEventDispatcher;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;

	import com.hurlant.crypto.hash.HMAC;
	import com.hurlant.crypto.hash.SHA1;
	import com.hurlant.crypto.Crypto;
	import com.hurlant.util.Hex;
	import flash.utils.ByteArray;

    public class SMS extends Object {

        public static var netStatusBanner:Label;
        public static var my_SMS:SMS;
        private var cacheRoundNum:int;
        private var cacheCategoryIndex:int;
        private var cacheQuestionIndex:int;
        private var cacheAnswerIndex:int;
        private var cacheDelayedAnswer:Boolean;
        public function SMS()
        {
            super();
        }

        public static function init(netBanner:Label):void {
            netStatusBanner = netBanner;
            netStatusBanner.text="SMS init called";
            my_SMS = new SMS();
        }

        public static function sendQuestionBegin(roundNum:int, categoryIndex:int, questionIndex:int, answerIndex:int, delayedAnswer:Boolean):void {
            my_SMS.my_SendQuestionBegin(roundNum, categoryIndex, questionIndex, answerIndex, delayedAnswer);
        }
        public function my_SendQuestionBegin(roundNum:int, categoryIndex:int, questionIndex:int, answerIndex:int, delayedAnswer:Boolean):void {
            cacheRoundNum = roundNum;
            cacheCategoryIndex = categoryIndex;
            cacheQuestionIndex = questionIndex;
            cacheAnswerIndex = answerIndex;
            cacheDelayedAnswer = delayedAnswer;
            if (Prefs.smsEnabled) {
                var variables:URLVariables = new URLVariables();
                variables.round = roundNum;
                variables.category = categoryIndex+1;
                variables.questionIndex = questionIndex+1;
                if (delayedAnswer) {
                    variables.answer = "delayed";
                } else {
                    variables.answer = answerIndex+1;
                }

                var payload:String = new String();

                payload = payload.concat(variables.round, variables.category,
                    variables.questionIndex, variables.answer);

                trace("Payload: "+payload);
                variables.hmac = hexHmacSha1(payload, Prefs.smsHMAC);
                trace("Signature: "+variables.hmac);

                var request:URLRequest = new URLRequest(Prefs.smsBase+"questionBegin");
                request.data = variables;

                var loader:URLLoader = new URLLoader();
                configureListeners(loader);
                try {
                    netStatusBanner.text = "Sending questionBegin...";
                    netStatusBanner.setStyle("color",  0xff0000);
                    loader.load(request);
                } catch (error:Error) {
                    trace("Error loading "+request.url+": "+error);
                    netStatusBanner.text = "Error sending questionBegin.";
                    netStatusBanner.setStyle("color",  0xff0000);
                }
            }
        }
        public static function resendQuestionBegin():void {
            my_SMS.my_ResendQuestionBegin();
        }
        public function my_ResendQuestionBegin():void {
            my_SendQuestionBegin(cacheRoundNum, cacheCategoryIndex, cacheQuestionIndex, cacheAnswerIndex, cacheDelayedAnswer);
        }
        public static function sendQuestionEnd(answerIndex:int, delayedAnswer:Boolean):void {
            my_SMS.my_SendQuestionEnd(answerIndex, delayedAnswer);
        }
        public function my_SendQuestionEnd(answerIndex:int, delayedAnswer:Boolean):void {
            cacheAnswerIndex = answerIndex;
            cacheDelayedAnswer = delayedAnswer;
            if (Prefs.smsEnabled) {
                var request:URLRequest = new URLRequest(Prefs.smsBase+"questionEnd");
                if (delayedAnswer) {
                    var variables:URLVariables = new URLVariables();
                    variables.answer = answerIndex+1;
                    request.data = variables;
                }
                var loader:URLLoader = new URLLoader();
                configureListeners(loader);
                try {
                    //sendToURL(request);
                    netStatusBanner.text = "Sending questionEnd...";
                    netStatusBanner.setStyle("color",  0xff0000);
                    loader.load(request);
                } catch (error:Error) {
                    trace("Error loading "+request.url+": "+error);
                    netStatusBanner.text = "Error sending questionEnd.";
                    netStatusBanner.setStyle("color",  0xff0000);
                }
            }
        }
        public static function resendQuestionEnd():void {
            my_SMS.my_ResendQuestionEnd();
        }
        public function my_ResendQuestionEnd():void {
            my_SendQuestionEnd(cacheAnswerIndex, cacheDelayedAnswer);
        }
		private function hexHmacSha1(payload:String, key:String):String {
			var payloadArray:ByteArray = Hex.toArray(Hex.fromString(payload));
			var keyArray:ByteArray = Hex.toArray(Hex.fromString(key));

			var hmac:HMAC = new HMAC(new SHA1());
			var digestArray:ByteArray = hmac.compute(keyArray, payloadArray);

			return Hex.fromArray(digestArray);
		}
		private function configureListeners(dispatcher:IEventDispatcher):void {
			dispatcher.addEventListener(Event.COMPLETE, completeHandler);
			dispatcher.addEventListener(Event.OPEN, openHandler);
			dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
		}

		private function completeHandler(event:Event):void {
			var loader:URLLoader = URLLoader(event.target);
			trace("completeHandler: " + loader.data);
			var pattern:RegExp = /[\r\n]+/g;
			netStatusBanner.htmlText = loader.data.replace(pattern,"");
            netStatusBanner.setStyle("color",  0x00ff00);
		}

		private function openHandler(event:Event):void {
			trace("openHandler: " + event);
		}

		private function progressHandler(event:ProgressEvent):void {
			trace("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
		}

		private function securityErrorHandler(event:SecurityErrorEvent):void {
			trace("securityErrorHandler: " + event);
		}

		private function httpStatusHandler(event:HTTPStatusEvent):void {
			trace("httpStatusHandler: " + event);
			netStatusBanner.text = event.toString();
		}

		private function ioErrorHandler(event:IOErrorEvent):void {
			trace("ioErrorHandler: " + event);
			netStatusBanner.text = "IO Error: "+event;
		}
    }

}

