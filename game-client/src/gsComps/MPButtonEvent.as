package gsComps {

    import org.wiiflash.events.ButtonEvent;
    import flash.events.Event;

    public class MPButtonEvent extends Event {
        // MPButtonEvent is a ButtonEvent with an extra id field to identify which controller sent the event.

        public var id : int;
        public var state:Boolean;

        public function MPButtonEvent(myId:int, event:ButtonEvent)
        {
            super(event.type, event.bubbles, event.cancelable);
            id = myId;
            state = event.state;
        }

    }

}

