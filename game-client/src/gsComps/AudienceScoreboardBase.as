﻿package gsComps {
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.data.DataProvider;
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.IEventDispatcher;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.errors.IOError;
	import flash.events.Event;
	
	import flash.display.BlendMode;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import fl.controls.listClasses.CellRenderer;
	
	public class AudienceScoreboard extends DataGrid {
		
		private var handleCol:DataGridColumn = new DataGridColumn("handle");
		private var scoreCol:DataGridColumn = new DataGridColumn("score");
		
		private var textFormat:TextFormat = new TextFormat();
		private var headerTextFormat:TextFormat = new TextFormat();
		
		private var netStatusBanner:NetStatusBanner = new NetStatusBanner();
		private var url:String;
	
		public function Scoreboard(url:String) {
			this.url = url;
			
			this.blendMode = BlendMode.LAYER;
			this.width = 400;
			this.height = 470;
			this.x = 312;
			this.y = 50;
			this.headerHeight = 38;
			this.rowHeight = 34;
			
			handleCol.width = 300;
			handleCol.headerText = "Name";
			
			scoreCol.width = 100;
			scoreCol.headerText = "Score";

			this.columns = [handleCol, scoreCol];
			
			textFormat.align = TextFormatAlign.CENTER;
			textFormat.color = 0x000000;
			textFormat.size = 28;
			textFormat.bold = false;
			textFormat.font = "Trebuchet MS";

			headerTextFormat.align = TextFormatAlign.CENTER;
			headerTextFormat.color = 0xCCCCFF;
			headerTextFormat.size = 28;
			headerTextFormat.bold = true;
			headerTextFormat.font = "Trebuchet MS";
			
			netStatusBanner.width = 396;
			netStatusBanner.x = 2;
			netStatusBanner.y = 456;
			this.addChild(netStatusBanner);
			
			this.setStyle("headerTextFormat", headerTextFormat);
			
//			this.setRendererStyle("textFormat", textFormat);
			
		}
		public function clear():void {
			this.removeAll();
		}
		public function update():void {
			var request:URLRequest = new URLRequest(url);
			var loader:URLLoader = new URLLoader();
			configureListeners(loader);
			try {
				netStatusBanner.text = "Requesting scoreboard...";
				loader.load(request);
			} catch (error:Error) {
				trace("Error loading "+request.url+": "+error);
				netStatusBanner.text = "Error sending questionBegin.";
			}
		}
		private function configureListeners(dispatcher:IEventDispatcher):void {
            dispatcher.addEventListener(Event.COMPLETE, completeHandler);
            dispatcher.addEventListener(Event.OPEN, openHandler);
            dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
            dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
            dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
        }

        private function completeHandler(event:Event):void {
            var loader:URLLoader = URLLoader(event.target);
            trace("completeHandler: " + loader.data);
			netStatusBanner.text = "Scores loaded.";
			
			var scoreData:XML = new XML(loader.data);
			var scoreDP:DataProvider = new DataProvider(scoreData);
			this.dataProvider = scoreDP;
        }

        private function openHandler(event:Event):void {
            trace("openHandler: " + event);
        }

        private function progressHandler(event:ProgressEvent):void {
            trace("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
        }

        private function securityErrorHandler(event:SecurityErrorEvent):void {
            trace("securityErrorHandler: " + event);
        }

        private function httpStatusHandler(event:HTTPStatusEvent):void {
            trace("httpStatusHandler: " + event);
			netStatusBanner.text = event.toString();
        }

        private function ioErrorHandler(event:IOErrorEvent):void {
            trace("ioErrorHandler: " + event);
			netStatusBanner.text = "IO Error: "+event;
        }		
	}
}
