package gsComps {

    import org.wiiflash.*;
    import org.wiiflash.events.*;
    import flash.errors.IOError;
    import flash.events.Event;
    
    public class Player extends Object {
        
        [Bindable] public var name:String = "";
        [Bindable] public var score:int = 0;
        

        private var wiimote:Wiimote;
        private var wiiListener:Function;
        private var id:int; // Player number

        public function Player(myId:int, myListener:Function)
        {
            super();
            try {
                wiimote = new Wiimote();
                wiimote.addEventListener( ButtonEvent.A_PRESS, onPress);
                wiimote.addEventListener( ButtonEvent.UP_PRESS, onPress);
                wiimote.addEventListener( ButtonEvent.RIGHT_PRESS, onPress);
                wiimote.addEventListener( ButtonEvent.DOWN_PRESS, onPress);
                wiimote.addEventListener( ButtonEvent.LEFT_PRESS, onPress);
                wiimote.connect();
            } catch (error:Error) {
                //Eat error
                trace("Wiiflash server not found");
            }
            this.id = myId;
            wiiListener = myListener;
        }

        public function setLamp(lampState:int):void {
            try {
                wiimote.leds = lampState;
            } catch (error:Error) {
                // Eat error because it's probably due to wiiserver not running.
            }
        }

        private function onConnect(event:Event):void {
        }

        private function onPress(event:ButtonEvent):void {
            //Tag event with id.
            wiiListener(new MPButtonEvent(id,event));
        }
    }

}

