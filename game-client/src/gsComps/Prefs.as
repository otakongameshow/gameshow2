package gsComps {

    public class Prefs extends Object {

        public static var smsEnabled:Boolean;
        public static var smsBase:String;
        public static var smsHMAC:String;
        public static var previewEnabled:Boolean;
        public static var previewMethod:String;
        public static var previewPath:String;
        public function Prefs()
        {
            super();
        }
        public static function init():void {
            smsEnabled = false;
            smsBase = "http://keyframe.cjas.org/gameshow/flash/";
            smsHMAC = "otakon2008g";
            previewEnabled = false;
            previewMethod = "local"; // Or "network"
            previewPath = "/Users/jhsu/Sites/q.txt"
        }
        
        public static function setFromConfig(conf:XMLList):void {
            smsEnabled = conf.smsEnabled == "true";
            if (conf.smsBase != undefined) {
                smsBase = conf.smsBase;
            }
            if (conf.smsHMAC != undefined) {
                smsHMAC = conf.smsHMAC;
            }
            previewEnabled = conf.previewEnabled == "true";
            if (conf.previewMethod != undefined) {
                previewMethod = conf.previewMethod;
            }
            if (conf.previewPath != undefined) {
                previewPath = conf.previewPath;
            }
            
            tracePrefs();
        }
        
        public static function tracePrefs():void {
            trace ("smsEnabled = " + smsEnabled);
            trace ("smsBase = " + smsBase);
            trace ("smsHMAC = " + smsHMAC);
            trace ("previewEnabled = " + previewEnabled);
            trace ("previewMethod = " + previewMethod);
            trace ("previewPath = " + previewPath);
        }

    }

}

